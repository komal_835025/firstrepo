
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RaceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RaceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Malay"/>
 *     &lt;enumeration value="Chinese"/>
 *     &lt;enumeration value="Indian"/>
 *     &lt;enumeration value="Others"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RaceType")
@XmlEnum
public enum RaceType {

    @XmlEnumValue("Malay")
    MALAY("Malay"),
    @XmlEnumValue("Chinese")
    CHINESE("Chinese"),
    @XmlEnumValue("Indian")
    INDIAN("Indian"),
    @XmlEnumValue("Others")
    OTHERS("Others");
    private final String value;

    RaceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RaceType fromValue(String v) {
        for (RaceType c: RaceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
