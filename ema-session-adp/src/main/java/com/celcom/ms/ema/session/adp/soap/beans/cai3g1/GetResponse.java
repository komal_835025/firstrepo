
package com.celcom.ms.ema.session.adp.soap.beans.cai3g1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv.GetRespSubscriptionType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MOId" type="{http://schemas.ericsson.com/cai3g1.2/}AnyMOIdType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MOAttributes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}getResponseCELEMASUB"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "moId",
    "moAttributes"
})
@XmlRootElement(name = "GetResponse")
public class GetResponse {

    @XmlElement(name = "MOId")
    protected List<AnyMOIdType> moId;
    @XmlElement(name = "MOAttributes")
    protected GetResponse.MOAttributes moAttributes;

    /**
     * Gets the value of the moId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMOId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AnyMOIdType }
     * 
     * 
     */
    public List<AnyMOIdType> getMOId() {
        if (moId == null) {
            moId = new ArrayList<AnyMOIdType>();
        }
        return this.moId;
    }

    /**
     * Gets the value of the moAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link GetResponse.MOAttributes }
     *     
     */
    public GetResponse.MOAttributes getMOAttributes() {
        return moAttributes;
    }

    /**
     * Sets the value of the moAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetResponse.MOAttributes }
     *     
     */
    public void setMOAttributes(GetResponse.MOAttributes value) {
        this.moAttributes = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}getResponseCELEMASUB"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "getResponseCELEMASUB"
    })
    public static class MOAttributes {

        @XmlElement(namespace = "http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", required = true)
        protected GetRespSubscriptionType getResponseCELEMASUB;

        /**
         * Gets the value of the getResponseCELEMASUB property.
         * 
         * @return
         *     possible object is
         *     {@link GetRespSubscriptionType }
         *     
         */
        public GetRespSubscriptionType getGetResponseCELEMASUB() {
            return getResponseCELEMASUB;
        }

        /**
         * Sets the value of the getResponseCELEMASUB property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetRespSubscriptionType }
         *     
         */
        public void setGetResponseCELEMASUB(GetRespSubscriptionType value) {
            this.getResponseCELEMASUB = value;
        }

    }

}
