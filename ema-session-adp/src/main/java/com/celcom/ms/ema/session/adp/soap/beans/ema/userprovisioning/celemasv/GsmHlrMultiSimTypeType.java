
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrMultiSimTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrMultiSimTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="imsis" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrLinkedImsiTypeType" maxOccurs="unbounded"/>
 *         &lt;element name="mch" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="acimsi">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="6"/>
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mMsisdn">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="5"/>
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="delall" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="initFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrMultiSimTypeType", propOrder = {
    "imsis",
    "mch",
    "acimsi",
    "mMsisdn",
    "delall",
    "initFlag"
})
public class GsmHlrMultiSimTypeType {

    @XmlElement(required = true)
    protected List<GsmHlrLinkedImsiTypeType> imsis;
    @XmlElement(required = true)
    protected String mch;
    @XmlElement(required = true)
    protected String acimsi;
    @XmlElement(required = true)
    protected String mMsisdn;
    protected Integer delall;
    protected boolean initFlag;

    /**
     * Gets the value of the imsis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the imsis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImsis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrLinkedImsiTypeType }
     * 
     * 
     */
    public List<GsmHlrLinkedImsiTypeType> getImsis() {
        if (imsis == null) {
            imsis = new ArrayList<GsmHlrLinkedImsiTypeType>();
        }
        return this.imsis;
    }

    /**
     * Gets the value of the mch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMch() {
        return mch;
    }

    /**
     * Sets the value of the mch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMch(String value) {
        this.mch = value;
    }

    /**
     * Gets the value of the acimsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcimsi() {
        return acimsi;
    }

    /**
     * Sets the value of the acimsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcimsi(String value) {
        this.acimsi = value;
    }

    /**
     * Gets the value of the mMsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMMsisdn() {
        return mMsisdn;
    }

    /**
     * Sets the value of the mMsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMMsisdn(String value) {
        this.mMsisdn = value;
    }

    /**
     * Gets the value of the delall property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDelall() {
        return delall;
    }

    /**
     * Sets the value of the delall property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDelall(Integer value) {
        this.delall = value;
    }

    /**
     * Gets the value of the initFlag property.
     * 
     */
    public boolean isInitFlag() {
        return initFlag;
    }

    /**
     * Sets the value of the initFlag property.
     * 
     */
    public void setInitFlag(boolean value) {
        this.initFlag = value;
    }

}
