
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrServtTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrServtTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="servt">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gres" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrGresTypeType" minOccurs="0"/>
 *         &lt;element name="notf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="servt" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrServtTypeType", propOrder = {
    "servt",
    "gres",
    "notf"
})
public class GsmHlrServtTypeType {

    @XmlElement(required = true)
    protected String servt;
    protected String gres;
    protected Integer notf;
    @XmlAttribute(name = "servt", required = true)
    protected String servtAttr;

    /**
     * Gets the value of the servt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServt() {
        return servt;
    }

    /**
     * Sets the value of the servt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServt(String value) {
        this.servt = value;
    }

    /**
     * Gets the value of the gres property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGres() {
        return gres;
    }

    /**
     * Sets the value of the gres property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGres(String value) {
        this.gres = value;
    }

    /**
     * Gets the value of the notf property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNotf() {
        return notf;
    }

    /**
     * Sets the value of the notf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNotf(Integer value) {
        this.notf = value;
    }

    /**
     * Gets the value of the servtAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServtAttr() {
        return servtAttr;
    }

    /**
     * Sets the value of the servtAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServtAttr(String value) {
        this.servtAttr = value;
    }

}
