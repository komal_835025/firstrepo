
package com.celcom.ms.ema.session.adp.soap.beans.cai3g1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv.DeleteSubscriptionType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MOType" type="{http://schemas.ericsson.com/cai3g1.2/}MoType"/>
 *         &lt;element name="MOId" type="{http://schemas.ericsson.com/cai3g1.2/}AnyMOIdType"/>
 *         &lt;element name="MOAttributes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}deleteCELEMASUB"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "moType",
    "moId",
    "moAttributes"
})
@XmlRootElement(name = "Delete")
public class Delete {

    @XmlElement(name = "MOType", required = true)
    protected String moType;
    @XmlElement(name = "MOId", required = true)
    protected AnyMOIdType moId;
    @XmlElement(name = "MOAttributes")
    protected Delete.MOAttributes moAttributes;

    /**
     * Gets the value of the moType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMOType() {
        return moType;
    }

    /**
     * Sets the value of the moType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMOType(String value) {
        this.moType = value;
    }

    /**
     * Gets the value of the moId property.
     * 
     * @return
     *     possible object is
     *     {@link AnyMOIdType }
     *     
     */
    public AnyMOIdType getMOId() {
        return moId;
    }

    /**
     * Sets the value of the moId property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnyMOIdType }
     *     
     */
    public void setMOId(AnyMOIdType value) {
        this.moId = value;
    }

    /**
     * Gets the value of the moAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link Delete.MOAttributes }
     *     
     */
    public Delete.MOAttributes getMOAttributes() {
        return moAttributes;
    }

    /**
     * Sets the value of the moAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Delete.MOAttributes }
     *     
     */
    public void setMOAttributes(Delete.MOAttributes value) {
        this.moAttributes = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}deleteCELEMASUB"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "deleteCELEMASUB"
    })
    public static class MOAttributes {

        @XmlElement(namespace = "http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", required = true)
        protected DeleteSubscriptionType deleteCELEMASUB;

        /**
         * Gets the value of the deleteCELEMASUB property.
         * 
         * @return
         *     possible object is
         *     {@link DeleteSubscriptionType }
         *     
         */
        public DeleteSubscriptionType getDeleteCELEMASUB() {
            return deleteCELEMASUB;
        }

        /**
         * Sets the value of the deleteCELEMASUB property.
         * 
         * @param value
         *     allowed object is
         *     {@link DeleteSubscriptionType }
         *     
         */
        public void setDeleteCELEMASUB(DeleteSubscriptionType value) {
            this.deleteCELEMASUB = value;
        }

    }

}
