
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSubscriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSubscriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="msisdn" use="required" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType" />
 *       &lt;attribute name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubscriptionType")
public class GetSubscriptionType {

    @XmlAttribute(name = "msisdn", required = true)
    protected String msisdnAttr;
    @XmlAttribute(name = "imsi")
    protected String imsiAttr;

    /**
     * Gets the value of the msisdnAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnAttr() {
        return msisdnAttr;
    }

    /**
     * Sets the value of the msisdnAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnAttr(String value) {
        this.msisdnAttr = value;
    }

    /**
     * Gets the value of the imsiAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsiAttr() {
        return imsiAttr;
    }

    /**
     * Sets the value of the imsiAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsiAttr(String value) {
        this.imsiAttr = value;
    }

}
