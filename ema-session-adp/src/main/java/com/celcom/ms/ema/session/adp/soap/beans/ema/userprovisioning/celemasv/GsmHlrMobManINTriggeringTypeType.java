
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrMobManINTriggeringTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrMobManINTriggeringTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="detectionPoint">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gsa">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="3"/>
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="serviceKey">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2147483647"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="activationState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrActivationTypeType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="detectionPoint" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrMobManINTriggeringTypeType", propOrder = {
    "detectionPoint",
    "gsa",
    "serviceKey",
    "activationState"
})
public class GsmHlrMobManINTriggeringTypeType {

    protected int detectionPoint;
    @XmlElement(required = true)
    protected String gsa;
    protected int serviceKey;
    @XmlElement(required = true)
    protected String activationState;
    @XmlAttribute(name = "detectionPoint", required = true)
    protected BigInteger detectionPointAttr;

    /**
     * Gets the value of the detectionPoint property.
     * 
     */
    public int getDetectionPoint() {
        return detectionPoint;
    }

    /**
     * Sets the value of the detectionPoint property.
     * 
     */
    public void setDetectionPoint(int value) {
        this.detectionPoint = value;
    }

    /**
     * Gets the value of the gsa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGsa() {
        return gsa;
    }

    /**
     * Sets the value of the gsa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGsa(String value) {
        this.gsa = value;
    }

    /**
     * Gets the value of the serviceKey property.
     * 
     */
    public int getServiceKey() {
        return serviceKey;
    }

    /**
     * Sets the value of the serviceKey property.
     * 
     */
    public void setServiceKey(int value) {
        this.serviceKey = value;
    }

    /**
     * Gets the value of the activationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationState() {
        return activationState;
    }

    /**
     * Sets the value of the activationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationState(String value) {
        this.activationState = value;
    }

    /**
     * Gets the value of the detectionPointAttr property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDetectionPointAttr() {
        return detectionPointAttr;
    }

    /**
     * Sets the value of the detectionPointAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDetectionPointAttr(BigInteger value) {
        this.detectionPointAttr = value;
    }

}
