
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrGmlcAddTypeExtType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrGmlcAddTypeExtType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="gmlcAdd" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrGmlcAddTypeType" maxOccurs="unbounded"/>
 *         &lt;element name="hgmlcId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="hgmlcAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pprId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pprAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrGmlcAddTypeExtType", propOrder = {
    "gmlcAdd",
    "hgmlcId",
    "hgmlcAdd",
    "pprId",
    "pprAdd"
})
public class GsmHlrGmlcAddTypeExtType {

    @XmlElement(required = true)
    protected List<GsmHlrGmlcAddTypeType> gmlcAdd;
    @XmlElement(required = true)
    protected String hgmlcId;
    @XmlElement(required = true)
    protected String hgmlcAdd;
    @XmlElement(required = true)
    protected String pprId;
    @XmlElement(required = true)
    protected String pprAdd;

    /**
     * Gets the value of the gmlcAdd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gmlcAdd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGmlcAdd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrGmlcAddTypeType }
     * 
     * 
     */
    public List<GsmHlrGmlcAddTypeType> getGmlcAdd() {
        if (gmlcAdd == null) {
            gmlcAdd = new ArrayList<GsmHlrGmlcAddTypeType>();
        }
        return this.gmlcAdd;
    }

    /**
     * Gets the value of the hgmlcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHgmlcId() {
        return hgmlcId;
    }

    /**
     * Sets the value of the hgmlcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHgmlcId(String value) {
        this.hgmlcId = value;
    }

    /**
     * Gets the value of the hgmlcAdd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHgmlcAdd() {
        return hgmlcAdd;
    }

    /**
     * Sets the value of the hgmlcAdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHgmlcAdd(String value) {
        this.hgmlcAdd = value;
    }

    /**
     * Gets the value of the pprId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPprId() {
        return pprId;
    }

    /**
     * Sets the value of the pprId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPprId(String value) {
        this.pprId = value;
    }

    /**
     * Gets the value of the pprAdd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPprAdd() {
        return pprAdd;
    }

    /**
     * Sets the value of the pprAdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPprAdd(String value) {
        this.pprAdd = value;
    }

}
