
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrEaddTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrEaddTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eadd">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="3"/>
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gres" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrGresTypeType" minOccurs="0"/>
 *         &lt;element name="notf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="crel" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *       &lt;attribute name="eadd" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="crel" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrEaddTypeType", propOrder = {
    "eadd",
    "gres",
    "notf",
    "crel"
})
public class GsmHlrEaddTypeType {

    @XmlElement(required = true)
    protected String eadd;
    protected String gres;
    protected Integer notf;
    protected boolean crel;
    @XmlAttribute(name = "eadd", required = true)
    protected String eaddAttr;
    @XmlAttribute(name = "crel", required = true)
    protected boolean crelAttr;

    /**
     * Gets the value of the eadd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEadd() {
        return eadd;
    }

    /**
     * Sets the value of the eadd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEadd(String value) {
        this.eadd = value;
    }

    /**
     * Gets the value of the gres property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGres() {
        return gres;
    }

    /**
     * Sets the value of the gres property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGres(String value) {
        this.gres = value;
    }

    /**
     * Gets the value of the notf property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNotf() {
        return notf;
    }

    /**
     * Sets the value of the notf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNotf(Integer value) {
        this.notf = value;
    }

    /**
     * Gets the value of the crel property.
     * 
     */
    public boolean isCrel() {
        return crel;
    }

    /**
     * Sets the value of the crel property.
     * 
     */
    public void setCrel(boolean value) {
        this.crel = value;
    }

    /**
     * Gets the value of the eaddAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEaddAttr() {
        return eaddAttr;
    }

    /**
     * Sets the value of the eaddAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEaddAttr(String value) {
        this.eaddAttr = value;
    }

    /**
     * Gets the value of the crelAttr property.
     * 
     */
    public boolean isCrelAttr() {
        return crelAttr;
    }

    /**
     * Sets the value of the crelAttr property.
     * 
     */
    public void setCrelAttr(boolean value) {
        this.crelAttr = value;
    }

}
