package com.celcom.ms.ema.session.adp.util;

public interface Properties {

	String KEY_EMA_SESSION_CONTROL_LOGIN_USERNAME = "ema.session.control.login.username";

	String KEY_EMA_SESSION_CONTROL_LOGIN_PASSWORD = "ema.session.control.login.password";
		
}
