
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SetSubscriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SetSubscriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType"/>
 *         &lt;element name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" minOccurs="0"/>
 *         &lt;element name="iccid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nmsisdn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType" minOccurs="0"/>
 *         &lt;element name="nimsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" minOccurs="0"/>
 *         &lt;element name="usertype" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}usertypeType"/>
 *         &lt;element name="emaprofile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="prodlist" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aprodlist" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dprodlist" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="threegind" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}ThreegindicatorType"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cmpid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="language" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}LanguageType" minOccurs="0"/>
 *         &lt;element name="race" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}RaceType" minOccurs="0"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="packtype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="channel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billcycledate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nbillcycle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ordersource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sms" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="msisdn" use="required" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType" />
 *       &lt;attribute name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetSubscriptionType", propOrder = {
    "msisdn",
    "imsi",
    "iccid",
    "nmsisdn",
    "nimsi",
    "usertype",
    "emaprofile",
    "prodlist",
    "aprodlist",
    "dprodlist",
    "threegind",
    "id",
    "cmpid",
    "promo",
    "language",
    "race",
    "state",
    "region",
    "packtype",
    "channel",
    "billcycledate",
    "nbillcycle",
    "ordersource",
    "sms"
})
public class SetSubscriptionType {

    @XmlElement(required = true)
    protected String msisdn;
    protected String imsi;
    protected String iccid;
    protected String nmsisdn;
    protected String nimsi;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected UsertypeType usertype;
    @XmlElement(required = true)
    protected String emaprofile;
    @XmlElement(required = true)
    protected String prodlist;
    protected String aprodlist;
    protected String dprodlist;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ThreegindicatorType threegind;
    @XmlElement(required = true)
    protected String id;
    protected String cmpid;
    protected String promo;
    @XmlSchemaType(name = "string")
    protected LanguageType language;
    @XmlSchemaType(name = "string")
    protected RaceType race;
    protected String state;
    protected String region;
    protected String packtype;
    protected String channel;
    protected String billcycledate;
    protected String nbillcycle;
    protected String ordersource;
    protected String sms;
    @XmlAttribute(name = "msisdn", required = true)
    protected String msisdnAttr;
    @XmlAttribute(name = "imsi")
    protected String imsiAttr;

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the imsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * Sets the value of the imsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsi(String value) {
        this.imsi = value;
    }

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIccid() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIccid(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the nmsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmsisdn() {
        return nmsisdn;
    }

    /**
     * Sets the value of the nmsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmsisdn(String value) {
        this.nmsisdn = value;
    }

    /**
     * Gets the value of the nimsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNimsi() {
        return nimsi;
    }

    /**
     * Sets the value of the nimsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNimsi(String value) {
        this.nimsi = value;
    }

    /**
     * Gets the value of the usertype property.
     * 
     * @return
     *     possible object is
     *     {@link UsertypeType }
     *     
     */
    public UsertypeType getUsertype() {
        return usertype;
    }

    /**
     * Sets the value of the usertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link UsertypeType }
     *     
     */
    public void setUsertype(UsertypeType value) {
        this.usertype = value;
    }

    /**
     * Gets the value of the emaprofile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmaprofile() {
        return emaprofile;
    }

    /**
     * Sets the value of the emaprofile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmaprofile(String value) {
        this.emaprofile = value;
    }

    /**
     * Gets the value of the prodlist property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdlist() {
        return prodlist;
    }

    /**
     * Sets the value of the prodlist property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdlist(String value) {
        this.prodlist = value;
    }

    /**
     * Gets the value of the aprodlist property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAprodlist() {
        return aprodlist;
    }

    /**
     * Sets the value of the aprodlist property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAprodlist(String value) {
        this.aprodlist = value;
    }

    /**
     * Gets the value of the dprodlist property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDprodlist() {
        return dprodlist;
    }

    /**
     * Sets the value of the dprodlist property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDprodlist(String value) {
        this.dprodlist = value;
    }

    /**
     * Gets the value of the threegind property.
     * 
     * @return
     *     possible object is
     *     {@link ThreegindicatorType }
     *     
     */
    public ThreegindicatorType getThreegind() {
        return threegind;
    }

    /**
     * Sets the value of the threegind property.
     * 
     * @param value
     *     allowed object is
     *     {@link ThreegindicatorType }
     *     
     */
    public void setThreegind(ThreegindicatorType value) {
        this.threegind = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the cmpid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmpid() {
        return cmpid;
    }

    /**
     * Sets the value of the cmpid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmpid(String value) {
        this.cmpid = value;
    }

    /**
     * Gets the value of the promo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromo() {
        return promo;
    }

    /**
     * Sets the value of the promo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromo(String value) {
        this.promo = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link LanguageType }
     *     
     */
    public LanguageType getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link LanguageType }
     *     
     */
    public void setLanguage(LanguageType value) {
        this.language = value;
    }

    /**
     * Gets the value of the race property.
     * 
     * @return
     *     possible object is
     *     {@link RaceType }
     *     
     */
    public RaceType getRace() {
        return race;
    }

    /**
     * Sets the value of the race property.
     * 
     * @param value
     *     allowed object is
     *     {@link RaceType }
     *     
     */
    public void setRace(RaceType value) {
        this.race = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the packtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPacktype() {
        return packtype;
    }

    /**
     * Sets the value of the packtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPacktype(String value) {
        this.packtype = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannel(String value) {
        this.channel = value;
    }

    /**
     * Gets the value of the billcycledate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillcycledate() {
        return billcycledate;
    }

    /**
     * Sets the value of the billcycledate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillcycledate(String value) {
        this.billcycledate = value;
    }

    /**
     * Gets the value of the nbillcycle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNbillcycle() {
        return nbillcycle;
    }

    /**
     * Sets the value of the nbillcycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNbillcycle(String value) {
        this.nbillcycle = value;
    }

    /**
     * Gets the value of the ordersource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdersource() {
        return ordersource;
    }

    /**
     * Sets the value of the ordersource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdersource(String value) {
        this.ordersource = value;
    }

    /**
     * Gets the value of the sms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSms() {
        return sms;
    }

    /**
     * Sets the value of the sms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSms(String value) {
        this.sms = value;
    }

    /**
     * Gets the value of the msisdnAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnAttr() {
        return msisdnAttr;
    }

    /**
     * Sets the value of the msisdnAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnAttr(String value) {
        this.msisdnAttr = value;
    }

    /**
     * Gets the value of the imsiAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsiAttr() {
        return imsiAttr;
    }

    /**
     * Sets the value of the imsiAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsiAttr(String value) {
        this.imsiAttr = value;
    }

}
