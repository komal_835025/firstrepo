
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrQosTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrQosTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rc">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dc">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pc">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pt">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="9"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mt">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="31"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrQosTypeType", propOrder = {
    "rc",
    "dc",
    "pc",
    "pt",
    "mt"
})
public class GsmHlrQosTypeType {

    protected int rc;
    protected int dc;
    protected int pc;
    protected int pt;
    protected int mt;

    /**
     * Gets the value of the rc property.
     * 
     */
    public int getRc() {
        return rc;
    }

    /**
     * Sets the value of the rc property.
     * 
     */
    public void setRc(int value) {
        this.rc = value;
    }

    /**
     * Gets the value of the dc property.
     * 
     */
    public int getDc() {
        return dc;
    }

    /**
     * Sets the value of the dc property.
     * 
     */
    public void setDc(int value) {
        this.dc = value;
    }

    /**
     * Gets the value of the pc property.
     * 
     */
    public int getPc() {
        return pc;
    }

    /**
     * Sets the value of the pc property.
     * 
     */
    public void setPc(int value) {
        this.pc = value;
    }

    /**
     * Gets the value of the pt property.
     * 
     */
    public int getPt() {
        return pt;
    }

    /**
     * Sets the value of the pt property.
     * 
     */
    public void setPt(int value) {
        this.pt = value;
    }

    /**
     * Gets the value of the mt property.
     * 
     */
    public int getMt() {
        return mt;
    }

    /**
     * Sets the value of the mt property.
     * 
     */
    public void setMt(int value) {
        this.mt = value;
    }

}
