
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrEOCamelTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrEOCamelTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eoinci" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="eoick" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="etinci" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="etick" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gcso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sslo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mcso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gc2so">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mc2so">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tif">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gc3so">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mc3so">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gprsso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="osmsso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tsmsso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mmso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gc4so">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mc4so">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrEOCamelTypeType", propOrder = {
    "eoinci",
    "eoick",
    "etinci",
    "etick",
    "gcso",
    "sslo",
    "mcso",
    "gc2So",
    "mc2So",
    "tif",
    "gc3So",
    "mc3So",
    "gprsso",
    "osmsso",
    "tsmsso",
    "mmso",
    "gc4So",
    "mc4So"
})
public class GsmHlrEOCamelTypeType {

    protected Integer eoinci;
    protected Integer eoick;
    protected Integer etinci;
    protected Integer etick;
    protected int gcso;
    protected int sslo;
    protected int mcso;
    @XmlElement(name = "gc2so")
    protected int gc2So;
    @XmlElement(name = "mc2so")
    protected int mc2So;
    protected int tif;
    @XmlElement(name = "gc3so")
    protected int gc3So;
    @XmlElement(name = "mc3so")
    protected int mc3So;
    protected int gprsso;
    protected int osmsso;
    protected int tsmsso;
    protected int mmso;
    @XmlElement(name = "gc4so")
    protected int gc4So;
    @XmlElement(name = "mc4so")
    protected int mc4So;

    /**
     * Gets the value of the eoinci property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEoinci() {
        return eoinci;
    }

    /**
     * Sets the value of the eoinci property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEoinci(Integer value) {
        this.eoinci = value;
    }

    /**
     * Gets the value of the eoick property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEoick() {
        return eoick;
    }

    /**
     * Sets the value of the eoick property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEoick(Integer value) {
        this.eoick = value;
    }

    /**
     * Gets the value of the etinci property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEtinci() {
        return etinci;
    }

    /**
     * Sets the value of the etinci property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEtinci(Integer value) {
        this.etinci = value;
    }

    /**
     * Gets the value of the etick property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEtick() {
        return etick;
    }

    /**
     * Sets the value of the etick property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEtick(Integer value) {
        this.etick = value;
    }

    /**
     * Gets the value of the gcso property.
     * 
     */
    public int getGcso() {
        return gcso;
    }

    /**
     * Sets the value of the gcso property.
     * 
     */
    public void setGcso(int value) {
        this.gcso = value;
    }

    /**
     * Gets the value of the sslo property.
     * 
     */
    public int getSslo() {
        return sslo;
    }

    /**
     * Sets the value of the sslo property.
     * 
     */
    public void setSslo(int value) {
        this.sslo = value;
    }

    /**
     * Gets the value of the mcso property.
     * 
     */
    public int getMcso() {
        return mcso;
    }

    /**
     * Sets the value of the mcso property.
     * 
     */
    public void setMcso(int value) {
        this.mcso = value;
    }

    /**
     * Gets the value of the gc2So property.
     * 
     */
    public int getGc2So() {
        return gc2So;
    }

    /**
     * Sets the value of the gc2So property.
     * 
     */
    public void setGc2So(int value) {
        this.gc2So = value;
    }

    /**
     * Gets the value of the mc2So property.
     * 
     */
    public int getMc2So() {
        return mc2So;
    }

    /**
     * Sets the value of the mc2So property.
     * 
     */
    public void setMc2So(int value) {
        this.mc2So = value;
    }

    /**
     * Gets the value of the tif property.
     * 
     */
    public int getTif() {
        return tif;
    }

    /**
     * Sets the value of the tif property.
     * 
     */
    public void setTif(int value) {
        this.tif = value;
    }

    /**
     * Gets the value of the gc3So property.
     * 
     */
    public int getGc3So() {
        return gc3So;
    }

    /**
     * Sets the value of the gc3So property.
     * 
     */
    public void setGc3So(int value) {
        this.gc3So = value;
    }

    /**
     * Gets the value of the mc3So property.
     * 
     */
    public int getMc3So() {
        return mc3So;
    }

    /**
     * Sets the value of the mc3So property.
     * 
     */
    public void setMc3So(int value) {
        this.mc3So = value;
    }

    /**
     * Gets the value of the gprsso property.
     * 
     */
    public int getGprsso() {
        return gprsso;
    }

    /**
     * Sets the value of the gprsso property.
     * 
     */
    public void setGprsso(int value) {
        this.gprsso = value;
    }

    /**
     * Gets the value of the osmsso property.
     * 
     */
    public int getOsmsso() {
        return osmsso;
    }

    /**
     * Sets the value of the osmsso property.
     * 
     */
    public void setOsmsso(int value) {
        this.osmsso = value;
    }

    /**
     * Gets the value of the tsmsso property.
     * 
     */
    public int getTsmsso() {
        return tsmsso;
    }

    /**
     * Sets the value of the tsmsso property.
     * 
     */
    public void setTsmsso(int value) {
        this.tsmsso = value;
    }

    /**
     * Gets the value of the mmso property.
     * 
     */
    public int getMmso() {
        return mmso;
    }

    /**
     * Sets the value of the mmso property.
     * 
     */
    public void setMmso(int value) {
        this.mmso = value;
    }

    /**
     * Gets the value of the gc4So property.
     * 
     */
    public int getGc4So() {
        return gc4So;
    }

    /**
     * Sets the value of the gc4So property.
     * 
     */
    public void setGc4So(int value) {
        this.gc4So = value;
    }

    /**
     * Gets the value of the mc4So property.
     * 
     */
    public int getMc4So() {
        return mc4So;
    }

    /**
     * Sets the value of the mc4So property.
     * 
     */
    public void setMc4So(int value) {
        this.mc4So = value;
    }

}
