
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetResponseGsmHlrSteTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetResponseGsmHlrSteTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ste">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gmlcid">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gmlca" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetResponseGsmHlrSteTypeType", propOrder = {
    "ste",
    "gmlcid",
    "gmlca"
})
public class GetResponseGsmHlrSteTypeType {

    @XmlElement(required = true)
    protected String ste;
    @XmlElement(required = true)
    protected String gmlcid;
    protected String gmlca;

    /**
     * Gets the value of the ste property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSte() {
        return ste;
    }

    /**
     * Sets the value of the ste property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSte(String value) {
        this.ste = value;
    }

    /**
     * Gets the value of the gmlcid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGmlcid() {
        return gmlcid;
    }

    /**
     * Sets the value of the gmlcid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGmlcid(String value) {
        this.gmlcid = value;
    }

    /**
     * Gets the value of the gmlca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGmlca() {
        return gmlca;
    }

    /**
     * Sets the value of the gmlca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGmlca(String value) {
        this.gmlca = value;
    }

}
