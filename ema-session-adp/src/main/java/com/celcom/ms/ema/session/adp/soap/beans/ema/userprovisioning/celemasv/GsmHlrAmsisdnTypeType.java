
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrAmsisdnTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrAmsisdnTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amsisdn">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="5"/>
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bc">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="65534"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="4"/>
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="amsisdn" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrAmsisdnTypeType", propOrder = {
    "amsisdn",
    "bc",
    "bs"
})
public class GsmHlrAmsisdnTypeType {

    @XmlElement(required = true)
    protected String amsisdn;
    protected int bc;
    protected String bs;
    @XmlAttribute(name = "amsisdn", required = true)
    protected String amsisdnAttr;

    /**
     * Gets the value of the amsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmsisdn() {
        return amsisdn;
    }

    /**
     * Sets the value of the amsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmsisdn(String value) {
        this.amsisdn = value;
    }

    /**
     * Gets the value of the bc property.
     * 
     */
    public int getBc() {
        return bc;
    }

    /**
     * Sets the value of the bc property.
     * 
     */
    public void setBc(int value) {
        this.bc = value;
    }

    /**
     * Gets the value of the bs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBs() {
        return bs;
    }

    /**
     * Sets the value of the bs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBs(String value) {
        this.bs = value;
    }

    /**
     * Gets the value of the amsisdnAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmsisdnAttr() {
        return amsisdnAttr;
    }

    /**
     * Sets the value of the amsisdnAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmsisdnAttr(String value) {
        this.amsisdnAttr = value;
    }

}
