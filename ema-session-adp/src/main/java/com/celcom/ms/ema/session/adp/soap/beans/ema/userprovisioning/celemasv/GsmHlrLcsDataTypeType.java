
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrLcsDataTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrLcsDataTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="univ" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrUnivTypeType" minOccurs="0"/>
 *         &lt;element name="crel" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCrelCunrlTypeType" minOccurs="0"/>
 *         &lt;element name="cunrl" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCrelCunrlTypeType" minOccurs="0"/>
 *         &lt;element name="plmno" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrPlmnoTypeType" minOccurs="0"/>
 *         &lt;element name="mocl" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrMoclTypeType" minOccurs="0"/>
 *         &lt;element name="eadd" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrEaddTypeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="servt" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrServtTypeType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrLcsDataTypeType", propOrder = {
    "univ",
    "crel",
    "cunrl",
    "plmno",
    "mocl",
    "eadd",
    "servt"
})
public class GsmHlrLcsDataTypeType {

    protected GsmHlrUnivTypeType univ;
    protected GsmHlrCrelCunrlTypeType crel;
    protected GsmHlrCrelCunrlTypeType cunrl;
    protected GsmHlrPlmnoTypeType plmno;
    protected GsmHlrMoclTypeType mocl;
    protected List<GsmHlrEaddTypeType> eadd;
    protected List<GsmHlrServtTypeType> servt;

    /**
     * Gets the value of the univ property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrUnivTypeType }
     *     
     */
    public GsmHlrUnivTypeType getUniv() {
        return univ;
    }

    /**
     * Sets the value of the univ property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrUnivTypeType }
     *     
     */
    public void setUniv(GsmHlrUnivTypeType value) {
        this.univ = value;
    }

    /**
     * Gets the value of the crel property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCrelCunrlTypeType }
     *     
     */
    public GsmHlrCrelCunrlTypeType getCrel() {
        return crel;
    }

    /**
     * Sets the value of the crel property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCrelCunrlTypeType }
     *     
     */
    public void setCrel(GsmHlrCrelCunrlTypeType value) {
        this.crel = value;
    }

    /**
     * Gets the value of the cunrl property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCrelCunrlTypeType }
     *     
     */
    public GsmHlrCrelCunrlTypeType getCunrl() {
        return cunrl;
    }

    /**
     * Sets the value of the cunrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCrelCunrlTypeType }
     *     
     */
    public void setCunrl(GsmHlrCrelCunrlTypeType value) {
        this.cunrl = value;
    }

    /**
     * Gets the value of the plmno property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrPlmnoTypeType }
     *     
     */
    public GsmHlrPlmnoTypeType getPlmno() {
        return plmno;
    }

    /**
     * Sets the value of the plmno property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrPlmnoTypeType }
     *     
     */
    public void setPlmno(GsmHlrPlmnoTypeType value) {
        this.plmno = value;
    }

    /**
     * Gets the value of the mocl property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrMoclTypeType }
     *     
     */
    public GsmHlrMoclTypeType getMocl() {
        return mocl;
    }

    /**
     * Sets the value of the mocl property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrMoclTypeType }
     *     
     */
    public void setMocl(GsmHlrMoclTypeType value) {
        this.mocl = value;
    }

    /**
     * Gets the value of the eadd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eadd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEadd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrEaddTypeType }
     * 
     * 
     */
    public List<GsmHlrEaddTypeType> getEadd() {
        if (eadd == null) {
            eadd = new ArrayList<GsmHlrEaddTypeType>();
        }
        return this.eadd;
    }

    /**
     * Gets the value of the servt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrServtTypeType }
     * 
     * 
     */
    public List<GsmHlrServtTypeType> getServt() {
        if (servt == null) {
            servt = new ArrayList<GsmHlrServtTypeType>();
        }
        return this.servt;
    }

}
