
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrLocationDataTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrLocationDataTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vlrAddress" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="6"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="msrn" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mscNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="lmsid" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="8"/>
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sgsnNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="6"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="locState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrLocationDataTypeType", propOrder = {
    "vlrAddress",
    "msrn",
    "mscNumber",
    "lmsid",
    "sgsnNumber",
    "locState"
})
public class GsmHlrLocationDataTypeType {

    protected String vlrAddress;
    protected String msrn;
    protected String mscNumber;
    protected String lmsid;
    protected String sgsnNumber;
    protected String locState;

    /**
     * Gets the value of the vlrAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVlrAddress() {
        return vlrAddress;
    }

    /**
     * Sets the value of the vlrAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVlrAddress(String value) {
        this.vlrAddress = value;
    }

    /**
     * Gets the value of the msrn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsrn() {
        return msrn;
    }

    /**
     * Sets the value of the msrn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsrn(String value) {
        this.msrn = value;
    }

    /**
     * Gets the value of the mscNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMscNumber() {
        return mscNumber;
    }

    /**
     * Sets the value of the mscNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMscNumber(String value) {
        this.mscNumber = value;
    }

    /**
     * Gets the value of the lmsid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLmsid() {
        return lmsid;
    }

    /**
     * Sets the value of the lmsid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLmsid(String value) {
        this.lmsid = value;
    }

    /**
     * Gets the value of the sgsnNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSgsnNumber() {
        return sgsnNumber;
    }

    /**
     * Sets the value of the sgsnNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSgsnNumber(String value) {
        this.sgsnNumber = value;
    }

    /**
     * Gets the value of the locState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocState() {
        return locState;
    }

    /**
     * Sets the value of the locState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocState(String value) {
        this.locState = value;
    }

}
