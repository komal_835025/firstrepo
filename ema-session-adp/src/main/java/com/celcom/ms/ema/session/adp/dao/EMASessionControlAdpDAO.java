/**
 * 
 */
package com.celcom.ms.ema.session.adp.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.celcom.ms.ema.session.adp.model.EMASessionControlResDTO;
import com.celcom.ms.ema.session.adp.soapclient.EMASessionControlAdpSOAPClient;

/**
 * @author 1583849
 *
 */
@Component
public class EMASessionControlAdpDAO {

	private static final Logger logger = LoggerFactory.getLogger(EMASessionControlAdpDAO.class);

	@Autowired
	EMASessionControlAdpSOAPClient emaSessionControlAdpSOAPClient;

	public EMASessionControlResDTO getEMAProvisionFrmSOAP() {
		logger.debug(" EMASessionControlAdpDAO : executing getEMAProvisionFrmSOAP() ");
		return emaSessionControlAdpSOAPClient.invokeEMASessionControl();
	}

}
