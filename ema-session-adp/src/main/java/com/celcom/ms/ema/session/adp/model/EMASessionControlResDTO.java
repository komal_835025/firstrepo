package com.celcom.ms.ema.session.adp.model;

public class EMASessionControlResDTO {

	private ResponseStatusDTO responseStatus;
	private String sessionId;
	private String baseSequenceId;

	/**
	 * @return the responseStatus
	 */
	public ResponseStatusDTO getResponseStatus() {
		return responseStatus;
	}

	/**
	 * @param responseStatus the responseStatus to set
	 */
	public void setResponseStatus(ResponseStatusDTO responseStatus) {
		this.responseStatus = responseStatus;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the baseSequenceId
	 */
	public String getBaseSequenceId() {
		return baseSequenceId;
	}

	/**
	 * @param baseSequenceId the baseSequenceId to set
	 */
	public void setBaseSequenceId(String baseSequenceId) {
		this.baseSequenceId = baseSequenceId;
	}

}
