
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ThreegindicatorType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ThreegindicatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Y"/>
 *     &lt;enumeration value="N"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ThreegindicatorType")
@XmlEnum
public enum ThreegindicatorType {

    Y,
    N;

    public String value() {
        return name();
    }

    public static ThreegindicatorType fromValue(String v) {
        return valueOf(v);
    }

}
