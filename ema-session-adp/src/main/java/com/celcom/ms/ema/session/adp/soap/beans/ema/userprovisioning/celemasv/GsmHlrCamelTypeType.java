
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCamelTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCamelTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ctdp" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCamelTriggerDetectionPointTypeType" maxOccurs="40" minOccurs="0"/>
 *         &lt;element name="ccamel" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCamelConditionalTriggerTypeType" minOccurs="0"/>
 *         &lt;element name="octdp2GUI" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrOctdp2GUIType2Type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tctdp12GUI" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrTctdp12GUIType2Type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="eoinci" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="eoick" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="etinci" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="etick" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gcso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sslo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mcso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gc2so" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mc2so" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tif" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gc3so" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mc3so" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gprsso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="osmsso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tsmsso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mmso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gc4so" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mc4so" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCamelTypeType", propOrder = {
    "ctdp",
    "ccamel",
    "octdp2GUI",
    "tctdp12GUI",
    "eoinci",
    "eoick",
    "etinci",
    "etick",
    "gcso",
    "sslo",
    "mcso",
    "gc2So",
    "mc2So",
    "tif",
    "gc3So",
    "mc3So",
    "gprsso",
    "osmsso",
    "tsmsso",
    "mmso",
    "gc4So",
    "mc4So"
})
public class GsmHlrCamelTypeType {

    protected List<GsmHlrCamelTriggerDetectionPointTypeType> ctdp;
    protected GsmHlrCamelConditionalTriggerTypeType ccamel;
    protected List<GsmHlrOctdp2GUIType2Type> octdp2GUI;
    protected List<GsmHlrTctdp12GUIType2Type> tctdp12GUI;
    protected Integer eoinci;
    protected Integer eoick;
    protected Integer etinci;
    protected Integer etick;
    protected Integer gcso;
    protected Integer sslo;
    protected Integer mcso;
    @XmlElement(name = "gc2so")
    protected Integer gc2So;
    @XmlElement(name = "mc2so")
    protected Integer mc2So;
    protected Integer tif;
    @XmlElement(name = "gc3so")
    protected Integer gc3So;
    @XmlElement(name = "mc3so")
    protected Integer mc3So;
    protected Integer gprsso;
    protected Integer osmsso;
    protected Integer tsmsso;
    protected Integer mmso;
    @XmlElement(name = "gc4so")
    protected Integer gc4So;
    @XmlElement(name = "mc4so")
    protected Integer mc4So;

    /**
     * Gets the value of the ctdp property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ctdp property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCtdp().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrCamelTriggerDetectionPointTypeType }
     * 
     * 
     */
    public List<GsmHlrCamelTriggerDetectionPointTypeType> getCtdp() {
        if (ctdp == null) {
            ctdp = new ArrayList<GsmHlrCamelTriggerDetectionPointTypeType>();
        }
        return this.ctdp;
    }

    /**
     * Gets the value of the ccamel property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCamelConditionalTriggerTypeType }
     *     
     */
    public GsmHlrCamelConditionalTriggerTypeType getCcamel() {
        return ccamel;
    }

    /**
     * Sets the value of the ccamel property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCamelConditionalTriggerTypeType }
     *     
     */
    public void setCcamel(GsmHlrCamelConditionalTriggerTypeType value) {
        this.ccamel = value;
    }

    /**
     * Gets the value of the octdp2GUI property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the octdp2GUI property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOctdp2GUI().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrOctdp2GUIType2Type }
     * 
     * 
     */
    public List<GsmHlrOctdp2GUIType2Type> getOctdp2GUI() {
        if (octdp2GUI == null) {
            octdp2GUI = new ArrayList<GsmHlrOctdp2GUIType2Type>();
        }
        return this.octdp2GUI;
    }

    /**
     * Gets the value of the tctdp12GUI property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tctdp12GUI property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTctdp12GUI().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrTctdp12GUIType2Type }
     * 
     * 
     */
    public List<GsmHlrTctdp12GUIType2Type> getTctdp12GUI() {
        if (tctdp12GUI == null) {
            tctdp12GUI = new ArrayList<GsmHlrTctdp12GUIType2Type>();
        }
        return this.tctdp12GUI;
    }

    /**
     * Gets the value of the eoinci property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEoinci() {
        return eoinci;
    }

    /**
     * Sets the value of the eoinci property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEoinci(Integer value) {
        this.eoinci = value;
    }

    /**
     * Gets the value of the eoick property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEoick() {
        return eoick;
    }

    /**
     * Sets the value of the eoick property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEoick(Integer value) {
        this.eoick = value;
    }

    /**
     * Gets the value of the etinci property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEtinci() {
        return etinci;
    }

    /**
     * Sets the value of the etinci property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEtinci(Integer value) {
        this.etinci = value;
    }

    /**
     * Gets the value of the etick property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEtick() {
        return etick;
    }

    /**
     * Sets the value of the etick property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEtick(Integer value) {
        this.etick = value;
    }

    /**
     * Gets the value of the gcso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGcso() {
        return gcso;
    }

    /**
     * Sets the value of the gcso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGcso(Integer value) {
        this.gcso = value;
    }

    /**
     * Gets the value of the sslo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSslo() {
        return sslo;
    }

    /**
     * Sets the value of the sslo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSslo(Integer value) {
        this.sslo = value;
    }

    /**
     * Gets the value of the mcso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMcso() {
        return mcso;
    }

    /**
     * Sets the value of the mcso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMcso(Integer value) {
        this.mcso = value;
    }

    /**
     * Gets the value of the gc2So property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGc2So() {
        return gc2So;
    }

    /**
     * Sets the value of the gc2So property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGc2So(Integer value) {
        this.gc2So = value;
    }

    /**
     * Gets the value of the mc2So property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMc2So() {
        return mc2So;
    }

    /**
     * Sets the value of the mc2So property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMc2So(Integer value) {
        this.mc2So = value;
    }

    /**
     * Gets the value of the tif property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTif() {
        return tif;
    }

    /**
     * Sets the value of the tif property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTif(Integer value) {
        this.tif = value;
    }

    /**
     * Gets the value of the gc3So property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGc3So() {
        return gc3So;
    }

    /**
     * Sets the value of the gc3So property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGc3So(Integer value) {
        this.gc3So = value;
    }

    /**
     * Gets the value of the mc3So property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMc3So() {
        return mc3So;
    }

    /**
     * Sets the value of the mc3So property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMc3So(Integer value) {
        this.mc3So = value;
    }

    /**
     * Gets the value of the gprsso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGprsso() {
        return gprsso;
    }

    /**
     * Sets the value of the gprsso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGprsso(Integer value) {
        this.gprsso = value;
    }

    /**
     * Gets the value of the osmsso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOsmsso() {
        return osmsso;
    }

    /**
     * Sets the value of the osmsso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOsmsso(Integer value) {
        this.osmsso = value;
    }

    /**
     * Gets the value of the tsmsso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTsmsso() {
        return tsmsso;
    }

    /**
     * Sets the value of the tsmsso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTsmsso(Integer value) {
        this.tsmsso = value;
    }

    /**
     * Gets the value of the mmso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMmso() {
        return mmso;
    }

    /**
     * Sets the value of the mmso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMmso(Integer value) {
        this.mmso = value;
    }

    /**
     * Gets the value of the gc4So property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGc4So() {
        return gc4So;
    }

    /**
     * Sets the value of the gc4So property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGc4So(Integer value) {
        this.gc4So = value;
    }

    /**
     * Gets the value of the mc4So property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMc4So() {
        return mc4So;
    }

    /**
     * Sets the value of the mc4So property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMc4So(Integer value) {
        this.mc4So = value;
    }

}
