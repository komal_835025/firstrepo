
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCamelConditionalTriggerTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCamelConditionalTriggerTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="octdp2" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCCamelOctdp2TypeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tctdp12" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCCamelTctdp12TypeType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCamelConditionalTriggerTypeType", propOrder = {
    "octdp2",
    "tctdp12"
})
public class GsmHlrCamelConditionalTriggerTypeType {

    protected List<GsmHlrCCamelOctdp2TypeType> octdp2;
    protected List<GsmHlrCCamelTctdp12TypeType> tctdp12;

    /**
     * Gets the value of the octdp2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the octdp2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOctdp2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrCCamelOctdp2TypeType }
     * 
     * 
     */
    public List<GsmHlrCCamelOctdp2TypeType> getOctdp2() {
        if (octdp2 == null) {
            octdp2 = new ArrayList<GsmHlrCCamelOctdp2TypeType>();
        }
        return this.octdp2;
    }

    /**
     * Gets the value of the tctdp12 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tctdp12 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTctdp12().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrCCamelTctdp12TypeType }
     * 
     * 
     */
    public List<GsmHlrCCamelTctdp12TypeType> getTctdp12() {
        if (tctdp12 == null) {
            tctdp12 = new ArrayList<GsmHlrCCamelTctdp12TypeType>();
        }
        return this.tctdp12;
    }

}
