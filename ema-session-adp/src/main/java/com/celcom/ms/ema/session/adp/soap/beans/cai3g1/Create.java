
package com.celcom.ms.ema.session.adp.soap.beans.cai3g1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv.CreateSubscriptionType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MOType" type="{http://schemas.ericsson.com/cai3g1.2/}MoType"/>
 *         &lt;element name="MOId" type="{http://schemas.ericsson.com/cai3g1.2/}AnyMOIdType" minOccurs="0"/>
 *         &lt;element name="MOAttributes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}createCELEMASUB"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "moType",
    "moId",
    "moAttributes"
})
@XmlRootElement(name = "Create")
public class Create {

    @XmlElement(name = "MOType", required = true)
    protected String moType;
    @XmlElement(name = "MOId")
    protected AnyMOIdType moId;
    @XmlElement(name = "MOAttributes")
    protected Create.MOAttributes moAttributes;

    /**
     * Gets the value of the moType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMOType() {
        return moType;
    }

    /**
     * Sets the value of the moType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMOType(String value) {
        this.moType = value;
    }

    /**
     * Gets the value of the moId property.
     * 
     * @return
     *     possible object is
     *     {@link AnyMOIdType }
     *     
     */
    public AnyMOIdType getMOId() {
        return moId;
    }

    /**
     * Sets the value of the moId property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnyMOIdType }
     *     
     */
    public void setMOId(AnyMOIdType value) {
        this.moId = value;
    }

    /**
     * Gets the value of the moAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link Create.MOAttributes }
     *     
     */
    public Create.MOAttributes getMOAttributes() {
        return moAttributes;
    }

    /**
     * Sets the value of the moAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Create.MOAttributes }
     *     
     */
    public void setMOAttributes(Create.MOAttributes value) {
        this.moAttributes = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}createCELEMASUB"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "createCELEMASUB"
    })
    public static class MOAttributes {

        @XmlElement(namespace = "http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", required = true)
        protected CreateSubscriptionType createCELEMASUB;

        /**
         * Gets the value of the createCELEMASUB property.
         * 
         * @return
         *     possible object is
         *     {@link CreateSubscriptionType }
         *     
         */
        public CreateSubscriptionType getCreateCELEMASUB() {
            return createCELEMASUB;
        }

        /**
         * Sets the value of the createCELEMASUB property.
         * 
         * @param value
         *     allowed object is
         *     {@link CreateSubscriptionType }
         *     
         */
        public void setCreateCELEMASUB(CreateSubscriptionType value) {
            this.createCELEMASUB = value;
        }

    }

}
