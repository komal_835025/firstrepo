
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrIntIdTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrIntIdTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="intId0" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="intId1" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="intId2" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="intId3" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="intId4" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrIntIdTypeType", propOrder = {
    "intId0",
    "intId1",
    "intId2",
    "intId3",
    "intId4"
})
public class GsmHlrIntIdTypeType {

    protected Boolean intId0;
    protected Boolean intId1;
    protected Boolean intId2;
    protected Boolean intId3;
    protected Boolean intId4;

    /**
     * Gets the value of the intId0 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIntId0() {
        return intId0;
    }

    /**
     * Sets the value of the intId0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntId0(Boolean value) {
        this.intId0 = value;
    }

    /**
     * Gets the value of the intId1 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIntId1() {
        return intId1;
    }

    /**
     * Sets the value of the intId1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntId1(Boolean value) {
        this.intId1 = value;
    }

    /**
     * Gets the value of the intId2 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIntId2() {
        return intId2;
    }

    /**
     * Sets the value of the intId2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntId2(Boolean value) {
        this.intId2 = value;
    }

    /**
     * Gets the value of the intId3 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIntId3() {
        return intId3;
    }

    /**
     * Sets the value of the intId3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntId3(Boolean value) {
        this.intId3 = value;
    }

    /**
     * Gets the value of the intId4 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIntId4() {
        return intId4;
    }

    /**
     * Sets the value of the intId4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntId4(Boolean value) {
        this.intId4 = value;
    }

}
