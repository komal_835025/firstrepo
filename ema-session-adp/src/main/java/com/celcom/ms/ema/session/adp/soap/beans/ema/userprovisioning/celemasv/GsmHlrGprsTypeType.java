
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrGprsTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrGprsTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apnid">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pdpadd">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="39"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pdpid">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="eqosid">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="4095"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pdpch" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="7"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pdpty" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="activationState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrActivationTypeType" minOccurs="0"/>
 *         &lt;element name="qos" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrQosTypeType" minOccurs="0"/>
 *         &lt;element name="vpaa" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="pdpid" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrGprsTypeType", propOrder = {
    "apnid",
    "pdpadd",
    "pdpid",
    "eqosid",
    "pdpch",
    "pdpty",
    "activationState",
    "qos",
    "vpaa"
})
public class GsmHlrGprsTypeType {

    @XmlElement(required = true)
    protected String apnid;
    @XmlElement(required = true)
    protected String pdpadd;
    @XmlElement(required = true)
    protected String pdpid;
    protected int eqosid;
    protected String pdpch;
    protected String pdpty;
    protected String activationState;
    protected GsmHlrQosTypeType qos;
    protected Integer vpaa;
    @XmlAttribute(name = "pdpid", required = true)
    protected String pdpidAttr;

    /**
     * Gets the value of the apnid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApnid() {
        return apnid;
    }

    /**
     * Sets the value of the apnid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApnid(String value) {
        this.apnid = value;
    }

    /**
     * Gets the value of the pdpadd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdpadd() {
        return pdpadd;
    }

    /**
     * Sets the value of the pdpadd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdpadd(String value) {
        this.pdpadd = value;
    }

    /**
     * Gets the value of the pdpid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdpid() {
        return pdpid;
    }

    /**
     * Sets the value of the pdpid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdpid(String value) {
        this.pdpid = value;
    }

    /**
     * Gets the value of the eqosid property.
     * 
     */
    public int getEqosid() {
        return eqosid;
    }

    /**
     * Sets the value of the eqosid property.
     * 
     */
    public void setEqosid(int value) {
        this.eqosid = value;
    }

    /**
     * Gets the value of the pdpch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdpch() {
        return pdpch;
    }

    /**
     * Sets the value of the pdpch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdpch(String value) {
        this.pdpch = value;
    }

    /**
     * Gets the value of the pdpty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdpty() {
        return pdpty;
    }

    /**
     * Sets the value of the pdpty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdpty(String value) {
        this.pdpty = value;
    }

    /**
     * Gets the value of the activationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationState() {
        return activationState;
    }

    /**
     * Sets the value of the activationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationState(String value) {
        this.activationState = value;
    }

    /**
     * Gets the value of the qos property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrQosTypeType }
     *     
     */
    public GsmHlrQosTypeType getQos() {
        return qos;
    }

    /**
     * Sets the value of the qos property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrQosTypeType }
     *     
     */
    public void setQos(GsmHlrQosTypeType value) {
        this.qos = value;
    }

    /**
     * Gets the value of the vpaa property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVpaa() {
        return vpaa;
    }

    /**
     * Sets the value of the vpaa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVpaa(Integer value) {
        this.vpaa = value;
    }

    /**
     * Gets the value of the pdpidAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdpidAttr() {
        return pdpidAttr;
    }

    /**
     * Sets the value of the pdpidAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdpidAttr(String value) {
        this.pdpidAttr = value;
    }

}
