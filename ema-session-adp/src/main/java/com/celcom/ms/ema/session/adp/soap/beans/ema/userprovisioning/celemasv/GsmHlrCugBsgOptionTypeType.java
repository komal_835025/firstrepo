
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCugBsgOptionTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCugBsgOptionTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accessibility" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugAccessibilityTypeType" minOccurs="0"/>
 *         &lt;element name="preferentialCug" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ts10" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugSingleBsgOptionTypeType" minOccurs="0"/>
 *         &lt;element name="ts60" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugSingleBsgOptionTypeType" minOccurs="0"/>
 *         &lt;element name="tsd0" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugSingleBsgOptionTypeType" minOccurs="0"/>
 *         &lt;element name="bs20" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugSingleBsgOptionTypeType" minOccurs="0"/>
 *         &lt;element name="bs30" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugSingleBsgOptionTypeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCugBsgOptionTypeType", propOrder = {
    "accessibility",
    "preferentialCug",
    "ts10",
    "ts60",
    "tsd0",
    "bs20",
    "bs30"
})
public class GsmHlrCugBsgOptionTypeType {

    protected String accessibility;
    protected String preferentialCug;
    protected GsmHlrCugSingleBsgOptionTypeType ts10;
    protected GsmHlrCugSingleBsgOptionTypeType ts60;
    protected GsmHlrCugSingleBsgOptionTypeType tsd0;
    protected GsmHlrCugSingleBsgOptionTypeType bs20;
    protected GsmHlrCugSingleBsgOptionTypeType bs30;

    /**
     * Gets the value of the accessibility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessibility() {
        return accessibility;
    }

    /**
     * Sets the value of the accessibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessibility(String value) {
        this.accessibility = value;
    }

    /**
     * Gets the value of the preferentialCug property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferentialCug() {
        return preferentialCug;
    }

    /**
     * Sets the value of the preferentialCug property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferentialCug(String value) {
        this.preferentialCug = value;
    }

    /**
     * Gets the value of the ts10 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public GsmHlrCugSingleBsgOptionTypeType getTs10() {
        return ts10;
    }

    /**
     * Sets the value of the ts10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public void setTs10(GsmHlrCugSingleBsgOptionTypeType value) {
        this.ts10 = value;
    }

    /**
     * Gets the value of the ts60 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public GsmHlrCugSingleBsgOptionTypeType getTs60() {
        return ts60;
    }

    /**
     * Sets the value of the ts60 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public void setTs60(GsmHlrCugSingleBsgOptionTypeType value) {
        this.ts60 = value;
    }

    /**
     * Gets the value of the tsd0 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public GsmHlrCugSingleBsgOptionTypeType getTsd0() {
        return tsd0;
    }

    /**
     * Sets the value of the tsd0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public void setTsd0(GsmHlrCugSingleBsgOptionTypeType value) {
        this.tsd0 = value;
    }

    /**
     * Gets the value of the bs20 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public GsmHlrCugSingleBsgOptionTypeType getBs20() {
        return bs20;
    }

    /**
     * Sets the value of the bs20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public void setBs20(GsmHlrCugSingleBsgOptionTypeType value) {
        this.bs20 = value;
    }

    /**
     * Gets the value of the bs30 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public GsmHlrCugSingleBsgOptionTypeType getBs30() {
        return bs30;
    }

    /**
     * Sets the value of the bs30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCugSingleBsgOptionTypeType }
     *     
     */
    public void setBs30(GsmHlrCugSingleBsgOptionTypeType value) {
        this.bs30 = value;
    }

}
