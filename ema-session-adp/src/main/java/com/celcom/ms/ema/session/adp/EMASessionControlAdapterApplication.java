package com.celcom.ms.ema.session.adp;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EMASessionControlAdapterApplication {
	
	/*
	 * @Autowired private RefreshEndpoint refreshEndpoint;
	 */
	
	
	public static void main(String[] args) {
	
		SpringApplication.run(EMASessionControlAdapterApplication.class, args);
		
		
		//Runnable task = () -> {
			//System.out.println(" **********************  inside runnable ***************");
			/*RestTemplate restTemplate = new RestTemplate();
			restTemplate.setInterceptors(Collections.singletonList(new JsonMimeInterceptor()));
			Gson gson = new Gson();
			JsonElement jsonElement = gson.fromJson("{}", JsonElement.class);
			JsonObject jsonObject = jsonElement.getAsJsonObject();
			restTemplate.postForObject("http://localhost:9090/actuator/refresh",jsonObject,Object.class);
			//RefreshProperties refreshProps = new RefreshProperties();
			//refreshProps.refreshEndpoint();*/
			
			/*String curlCommand = "curl -X POST http://10.1.59.42:30015/actuator/refresh";
			try {
				//System.out.println(" **********************  before curl execution ***************");
				Process p = Runtime.getRuntime().exec(curlCommand);
				//System.out.println(" **********************  after curl execution ***************");
			} catch (Exception ex) {
				System.out.println(ex);
			}
			//System.out.println(" **********************  after runnable ***************");
		};*/
		/*ScheduledExecutorService execService = Executors.newScheduledThreadPool(1);
		execService.scheduleAtFixedRate(task, 1, 600 , TimeUnit.SECONDS);*/

	}

}
