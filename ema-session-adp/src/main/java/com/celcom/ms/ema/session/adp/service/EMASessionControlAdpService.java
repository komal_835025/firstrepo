/**
 * 
 */
package com.celcom.ms.ema.session.adp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.celcom.ms.ema.session.adp.dao.EMASessionControlAdpDAO;
import com.celcom.ms.ema.session.adp.model.EMASessionControlResDTO;

/**
 * @author 1583849
 *
 */
@Component
public class EMASessionControlAdpService {

	private static final Logger logger = LoggerFactory.getLogger(EMASessionControlAdpService.class);

	@Autowired
	EMASessionControlAdpDAO emaSessionControlAdpDAO;

	public EMASessionControlResDTO getSessionId() {
		logger.debug(" EMASessionControlAdpService : executing getSessionId() ");
		return emaSessionControlAdpDAO.getEMAProvisionFrmSOAP();
	}

}
