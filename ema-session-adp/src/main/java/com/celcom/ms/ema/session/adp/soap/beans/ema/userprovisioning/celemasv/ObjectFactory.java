
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeleteCELEMASUB_QNAME = new QName("http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", "deleteCELEMASUB");
    private final static QName _GetResponseCELEMASUB_QNAME = new QName("http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", "getResponseCELEMASUB");
    private final static QName _CreateCELEMASUB_QNAME = new QName("http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", "createCELEMASUB");
    private final static QName _SetCELEMASUB_QNAME = new QName("http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", "setCELEMASUB");
    private final static QName _GetCELEMASUB_QNAME = new QName("http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", "getCELEMASUB");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateSubscriptionType }
     * 
     */
    public CreateSubscriptionType createCreateSubscriptionType() {
        return new CreateSubscriptionType();
    }

    /**
     * Create an instance of {@link SetSubscriptionType }
     * 
     */
    public SetSubscriptionType createSetSubscriptionType() {
        return new SetSubscriptionType();
    }

    /**
     * Create an instance of {@link GetSubscriptionType }
     * 
     */
    public GetSubscriptionType createGetSubscriptionType() {
        return new GetSubscriptionType();
    }

    /**
     * Create an instance of {@link DeleteSubscriptionType }
     * 
     */
    public DeleteSubscriptionType createDeleteSubscriptionType() {
        return new DeleteSubscriptionType();
    }

    /**
     * Create an instance of {@link GetRespSubscriptionType }
     * 
     */
    public GetRespSubscriptionType createGetRespSubscriptionType() {
        return new GetRespSubscriptionType();
    }

    /**
     * Create an instance of {@link GsmHlrSmsSpamTypeType }
     * 
     */
    public GsmHlrSmsSpamTypeType createGsmHlrSmsSpamTypeType() {
        return new GsmHlrSmsSpamTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrTctdp12GUIType2Type }
     * 
     */
    public GsmHlrTctdp12GUIType2Type createGsmHlrTctdp12GUIType2Type() {
        return new GsmHlrTctdp12GUIType2Type();
    }

    /**
     * Create an instance of {@link GsmHlrCamelTriggerDetectionPointTypeType }
     * 
     */
    public GsmHlrCamelTriggerDetectionPointTypeType createGsmHlrCamelTriggerDetectionPointTypeType() {
        return new GsmHlrCamelTriggerDetectionPointTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     * 
     */
    public GsmHlrFnumCfuCfbCfnrcActivationStateType createGsmHlrFnumCfuCfbCfnrcActivationStateType() {
        return new GsmHlrFnumCfuCfbCfnrcActivationStateType();
    }

    /**
     * Create an instance of {@link GsmHlrOctdp2GUIType2Type }
     * 
     */
    public GsmHlrOctdp2GUIType2Type createGsmHlrOctdp2GUIType2Type() {
        return new GsmHlrOctdp2GUIType2Type();
    }

    /**
     * Create an instance of {@link GsmHlrLinkedImsiTypeType }
     * 
     */
    public GsmHlrLinkedImsiTypeType createGsmHlrLinkedImsiTypeType() {
        return new GsmHlrLinkedImsiTypeType();
    }

    /**
     * Create an instance of {@link GetResponseGsmHlrSteTypeType }
     * 
     */
    public GetResponseGsmHlrSteTypeType createGetResponseGsmHlrSteTypeType() {
        return new GetResponseGsmHlrSteTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCrelCunrlTypeType }
     * 
     */
    public GsmHlrCrelCunrlTypeType createGsmHlrCrelCunrlTypeType() {
        return new GsmHlrCrelCunrlTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCfCfuCfbCfnrcSupplementaryServiceType }
     * 
     */
    public GsmHlrCfCfuCfbCfnrcSupplementaryServiceType createGsmHlrCfCfuCfbCfnrcSupplementaryServiceType() {
        return new GsmHlrCfCfuCfbCfnrcSupplementaryServiceType();
    }

    /**
     * Create an instance of {@link GsmHlrGmlcAddTypeExtType }
     * 
     */
    public GsmHlrGmlcAddTypeExtType createGsmHlrGmlcAddTypeExtType() {
        return new GsmHlrGmlcAddTypeExtType();
    }

    /**
     * Create an instance of {@link GsmHlrCCamelTctdp12TypeType }
     * 
     */
    public GsmHlrCCamelTctdp12TypeType createGsmHlrCCamelTctdp12TypeType() {
        return new GsmHlrCCamelTctdp12TypeType();
    }

    /**
     * Create an instance of {@link GsmHlrFnumCfnryDcfActivationStateType }
     * 
     */
    public GsmHlrFnumCfnryDcfActivationStateType createGsmHlrFnumCfnryDcfActivationStateType() {
        return new GsmHlrFnumCfnryDcfActivationStateType();
    }

    /**
     * Create an instance of {@link GsmHlrBarringSupplementaryServiceType }
     * 
     */
    public GsmHlrBarringSupplementaryServiceType createGsmHlrBarringSupplementaryServiceType() {
        return new GsmHlrBarringSupplementaryServiceType();
    }

    /**
     * Create an instance of {@link GsmHlrAmsisdnTypeType }
     * 
     */
    public GsmHlrAmsisdnTypeType createGsmHlrAmsisdnTypeType() {
        return new GsmHlrAmsisdnTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrGmlcAddTypeType }
     * 
     */
    public GsmHlrGmlcAddTypeType createGsmHlrGmlcAddTypeType() {
        return new GsmHlrGmlcAddTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrEOCamelTypeType }
     * 
     */
    public GsmHlrEOCamelTypeType createGsmHlrEOCamelTypeType() {
        return new GsmHlrEOCamelTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCugSingleBsgOptionTypeType }
     * 
     */
    public GsmHlrCugSingleBsgOptionTypeType createGsmHlrCugSingleBsgOptionTypeType() {
        return new GsmHlrCugSingleBsgOptionTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCfCfnryDcfSupplementaryServiceType }
     * 
     */
    public GsmHlrCfCfnryDcfSupplementaryServiceType createGsmHlrCfCfnryDcfSupplementaryServiceType() {
        return new GsmHlrCfCfnryDcfSupplementaryServiceType();
    }

    /**
     * Create an instance of {@link GsmHlrClosedUserGroupTypeType }
     * 
     */
    public GsmHlrClosedUserGroupTypeType createGsmHlrClosedUserGroupTypeType() {
        return new GsmHlrClosedUserGroupTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrEaddTypeType }
     * 
     */
    public GsmHlrEaddTypeType createGsmHlrEaddTypeType() {
        return new GsmHlrEaddTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCamelConditionalTriggerTypeType }
     * 
     */
    public GsmHlrCamelConditionalTriggerTypeType createGsmHlrCamelConditionalTriggerTypeType() {
        return new GsmHlrCamelConditionalTriggerTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrNamTypeType }
     * 
     */
    public GsmHlrNamTypeType createGsmHlrNamTypeType() {
        return new GsmHlrNamTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrGprsTypeType }
     * 
     */
    public GsmHlrGprsTypeType createGsmHlrGprsTypeType() {
        return new GsmHlrGprsTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrMoclTypeType }
     * 
     */
    public GsmHlrMoclTypeType createGsmHlrMoclTypeType() {
        return new GsmHlrMoclTypeType();
    }

    /**
     * Create an instance of {@link HlrResponseSubscriptionType }
     * 
     */
    public HlrResponseSubscriptionType createHlrResponseSubscriptionType() {
        return new HlrResponseSubscriptionType();
    }

    /**
     * Create an instance of {@link GsmHlrSmsSpamScAddsTypeType }
     * 
     */
    public GsmHlrSmsSpamScAddsTypeType createGsmHlrSmsSpamScAddsTypeType() {
        return new GsmHlrSmsSpamScAddsTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrMultiSimTypeType }
     * 
     */
    public GsmHlrMultiSimTypeType createGsmHlrMultiSimTypeType() {
        return new GsmHlrMultiSimTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCamelTypeType }
     * 
     */
    public GsmHlrCamelTypeType createGsmHlrCamelTypeType() {
        return new GsmHlrCamelTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCugBsgOptionTypeType }
     * 
     */
    public GsmHlrCugBsgOptionTypeType createGsmHlrCugBsgOptionTypeType() {
        return new GsmHlrCugBsgOptionTypeType();
    }

    /**
     * Create an instance of {@link FnxSubscriptionType }
     * 
     */
    public FnxSubscriptionType createFnxSubscriptionType() {
        return new FnxSubscriptionType();
    }

    /**
     * Create an instance of {@link GsmHlrServtTypeType }
     * 
     */
    public GsmHlrServtTypeType createGsmHlrServtTypeType() {
        return new GsmHlrServtTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCawSupplementaryServiceType }
     * 
     */
    public GsmHlrCawSupplementaryServiceType createGsmHlrCawSupplementaryServiceType() {
        return new GsmHlrCawSupplementaryServiceType();
    }

    /**
     * Create an instance of {@link GsmHlrUnivTypeType }
     * 
     */
    public GsmHlrUnivTypeType createGsmHlrUnivTypeType() {
        return new GsmHlrUnivTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrMobManINTriggeringTypeType }
     * 
     */
    public GsmHlrMobManINTriggeringTypeType createGsmHlrMobManINTriggeringTypeType() {
        return new GsmHlrMobManINTriggeringTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrIntIdTypeType }
     * 
     */
    public GsmHlrIntIdTypeType createGsmHlrIntIdTypeType() {
        return new GsmHlrIntIdTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrLocationDataTypeType }
     * 
     */
    public GsmHlrLocationDataTypeType createGsmHlrLocationDataTypeType() {
        return new GsmHlrLocationDataTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrPlmnoTypeType }
     * 
     */
    public GsmHlrPlmnoTypeType createGsmHlrPlmnoTypeType() {
        return new GsmHlrPlmnoTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrCCamelOctdp2TypeType }
     * 
     */
    public GsmHlrCCamelOctdp2TypeType createGsmHlrCCamelOctdp2TypeType() {
        return new GsmHlrCCamelOctdp2TypeType();
    }

    /**
     * Create an instance of {@link GsmHlrLcsDataTypeType }
     * 
     */
    public GsmHlrLcsDataTypeType createGsmHlrLcsDataTypeType() {
        return new GsmHlrLcsDataTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrQosTypeType }
     * 
     */
    public GsmHlrQosTypeType createGsmHlrQosTypeType() {
        return new GsmHlrQosTypeType();
    }

    /**
     * Create an instance of {@link GsmHlrFnumSpnActivationStateType }
     * 
     */
    public GsmHlrFnumSpnActivationStateType createGsmHlrFnumSpnActivationStateType() {
        return new GsmHlrFnumSpnActivationStateType();
    }

    /**
     * Create an instance of {@link GsmHlrSimpleActivationStateType }
     * 
     */
    public GsmHlrSimpleActivationStateType createGsmHlrSimpleActivationStateType() {
        return new GsmHlrSimpleActivationStateType();
    }

    /**
     * Create an instance of {@link GsmHlrCfSpnSupplementaryServiceType }
     * 
     */
    public GsmHlrCfSpnSupplementaryServiceType createGsmHlrCfSpnSupplementaryServiceType() {
        return new GsmHlrCfSpnSupplementaryServiceType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteSubscriptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", name = "deleteCELEMASUB")
    public JAXBElement<DeleteSubscriptionType> createDeleteCELEMASUB(DeleteSubscriptionType value) {
        return new JAXBElement<DeleteSubscriptionType>(_DeleteCELEMASUB_QNAME, DeleteSubscriptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRespSubscriptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", name = "getResponseCELEMASUB")
    public JAXBElement<GetRespSubscriptionType> createGetResponseCELEMASUB(GetRespSubscriptionType value) {
        return new JAXBElement<GetRespSubscriptionType>(_GetResponseCELEMASUB_QNAME, GetRespSubscriptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSubscriptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", name = "createCELEMASUB")
    public JAXBElement<CreateSubscriptionType> createCreateCELEMASUB(CreateSubscriptionType value) {
        return new JAXBElement<CreateSubscriptionType>(_CreateCELEMASUB_QNAME, CreateSubscriptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetSubscriptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", name = "setCELEMASUB")
    public JAXBElement<SetSubscriptionType> createSetCELEMASUB(SetSubscriptionType value) {
        return new JAXBElement<SetSubscriptionType>(_SetCELEMASUB_QNAME, SetSubscriptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSubscriptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/", name = "getCELEMASUB")
    public JAXBElement<GetSubscriptionType> createGetCELEMASUB(GetSubscriptionType value) {
        return new JAXBElement<GetSubscriptionType>(_GetCELEMASUB_QNAME, GetSubscriptionType.class, null, value);
    }

}
