
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HlrResponseSubscriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HlrResponseSubscriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType" minOccurs="0"/>
 *         &lt;element name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" minOccurs="0"/>
 *         &lt;element name="neDestAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="authd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="profileId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pdpcp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="csp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="amsisdn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrAmsisdnTypeType" maxOccurs="16" minOccurs="0"/>
 *         &lt;element name="camel" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCamelTypeType" minOccurs="0"/>
 *         &lt;element name="eoCamel" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrEOCamelTypeType" minOccurs="0"/>
 *         &lt;element name="mmintMo" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrMobManINTriggeringTypeType" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="closedUserGroup" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrClosedUserGroupTypeType" maxOccurs="10" minOccurs="0"/>
 *         &lt;element name="cugBsgOption" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugBsgOptionTypeType" minOccurs="0"/>
 *         &lt;element name="removeReferences" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="gprs" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrGprsTypeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nam" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrNamTypeType" minOccurs="0"/>
 *         &lt;element name="baic" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrBarringSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="baoc" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrBarringSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="boic" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrBarringSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="bicro" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrBarringSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="boiexh" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrBarringSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="cfu" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCfCfuCfbCfnrcSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="cfb" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCfCfuCfbCfnrcSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="cfnrc" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCfCfuCfbCfnrcSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="cfnry" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCfCfnryDcfSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="dcf" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCfCfnryDcfSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="spn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCfSpnSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="caw" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCawSupplementaryServiceType" minOccurs="0"/>
 *         &lt;element name="acc" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="aoc" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs21" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs22" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs23" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs24" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs25" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs26" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs2f" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs2g" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs31" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs32" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs33" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs34" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs3f" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs3g" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="capl" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cat" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="254"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="clip" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="clir" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="colp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="colr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dbsg" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="hold" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="hpn" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ici" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mpty" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="oba" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obopre" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obopri" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obrf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obssm" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="99"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obzi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obzo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="oin" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="oick" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="osb1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="osb2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="osb3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="osb4" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ofa" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="511"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pai" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="regser" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="65534"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pici" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pici2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pici3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pwd" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="4"/>
 *               &lt;maxLength value="11"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="socb" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="socfb" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="socfrc" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="socfry" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="socfu" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="soclip" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="soclir" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="socolp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sodcf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="soplcs" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sosdcf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="7"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="stype" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="127"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tick" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tin" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ts11" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ts21" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ts22" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ts61" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ts62" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="locationData" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrLocationDataTypeType" minOccurs="0"/>
 *         &lt;element name="vlrData" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="6"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="emlpp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="demlpp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="memlpp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tsmo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sendMemlppFirst" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="rsa" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="128"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ect" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obct" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obdct" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="obmct" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="schar" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="7"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ocsi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tcsi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gprcsi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="osmcsi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="vtcsi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mcsi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dcsi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tsmcsi" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mmint" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cug" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tsd1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="red" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="asl" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bsl" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="crel" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cunrl" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="plmno" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ttp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="univ" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="servtl" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ste" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="teardown" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gmlca" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrGmlcAddTypeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="gmlca12" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrGmlcAddTypeExtType" minOccurs="0"/>
 *         &lt;element name="lcsd" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrLcsDataTypeType" minOccurs="0"/>
 *         &lt;element name="steMo" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GetResponseGsmHlrSteTypeType" minOccurs="0"/>
 *         &lt;element name="prbt" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dualstat" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="submch" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dualstatnumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="5"/>
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="acr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="rtca" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="redmch" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mca" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="smspam" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ist" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="istcso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="istgso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="istvso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="msim" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="multiSim" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrMultiSimTypeType" minOccurs="0"/>
 *         &lt;element name="smsSpam" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrSmsSpamTypeType" minOccurs="0"/>
 *         &lt;element name="ard" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="imeisv" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="16"/>
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="smshr1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mderbt" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="imsi" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HlrResponseSubscriptionType", propOrder = {
    "msisdn",
    "imsi",
    "neDestAddress",
    "authd",
    "profileId",
    "pdpcp",
    "csp",
    "amsisdn",
    "camel",
    "eoCamel",
    "mmintMo",
    "closedUserGroup",
    "cugBsgOption",
    "removeReferences",
    "gprs",
    "nam",
    "baic",
    "baoc",
    "boic",
    "bicro",
    "boiexh",
    "cfu",
    "cfb",
    "cfnrc",
    "cfnry",
    "dcf",
    "spn",
    "caw",
    "acc",
    "aoc",
    "bs21",
    "bs22",
    "bs23",
    "bs24",
    "bs25",
    "bs26",
    "bs2F",
    "bs2G",
    "bs31",
    "bs32",
    "bs33",
    "bs34",
    "bs3F",
    "bs3G",
    "capl",
    "cat",
    "clip",
    "clir",
    "colp",
    "colr",
    "dbsg",
    "hold",
    "hpn",
    "ici",
    "mpty",
    "oba",
    "obi",
    "obo",
    "obopre",
    "obopri",
    "obrf",
    "obssm",
    "obr",
    "obzi",
    "obzo",
    "oin",
    "oick",
    "osb1",
    "osb2",
    "osb3",
    "osb4",
    "ofa",
    "pai",
    "regser",
    "pici",
    "pici2",
    "pici3",
    "pwd",
    "socb",
    "socfb",
    "socfrc",
    "socfry",
    "socfu",
    "soclip",
    "soclir",
    "socolp",
    "sodcf",
    "soplcs",
    "sosdcf",
    "stype",
    "tick",
    "tin",
    "ts11",
    "ts21",
    "ts22",
    "ts61",
    "ts62",
    "locationData",
    "vlrData",
    "emlpp",
    "demlpp",
    "memlpp",
    "tsmo",
    "sendMemlppFirst",
    "rsa",
    "obp",
    "ect",
    "obct",
    "obdct",
    "obmct",
    "schar",
    "ocsi",
    "tcsi",
    "gprcsi",
    "osmcsi",
    "vtcsi",
    "mcsi",
    "dcsi",
    "tsmcsi",
    "mmint",
    "cug",
    "tsd1",
    "red",
    "asl",
    "bsl",
    "crel",
    "cunrl",
    "plmno",
    "ttp",
    "univ",
    "servtl",
    "ste",
    "teardown",
    "gmlca",
    "gmlca12",
    "lcsd",
    "steMo",
    "prbt",
    "dualstat",
    "submch",
    "dualstatnumber",
    "acr",
    "rtca",
    "redmch",
    "mca",
    "smspam",
    "ist",
    "istcso",
    "istgso",
    "istvso",
    "msim",
    "multiSim",
    "smsSpam",
    "ard",
    "imeisv",
    "smshr1",
    "mderbt"
})
public class HlrResponseSubscriptionType {

    protected String msisdn;
    protected String imsi;
    protected String neDestAddress;
    protected String authd;
    protected Integer profileId;
    protected Integer pdpcp;
    protected Integer csp;
    protected List<GsmHlrAmsisdnTypeType> amsisdn;
    protected GsmHlrCamelTypeType camel;
    protected GsmHlrEOCamelTypeType eoCamel;
    protected List<GsmHlrMobManINTriggeringTypeType> mmintMo;
    protected List<GsmHlrClosedUserGroupTypeType> closedUserGroup;
    protected GsmHlrCugBsgOptionTypeType cugBsgOption;
    protected Boolean removeReferences;
    protected List<GsmHlrGprsTypeType> gprs;
    protected GsmHlrNamTypeType nam;
    protected GsmHlrBarringSupplementaryServiceType baic;
    protected GsmHlrBarringSupplementaryServiceType baoc;
    protected GsmHlrBarringSupplementaryServiceType boic;
    protected GsmHlrBarringSupplementaryServiceType bicro;
    protected GsmHlrBarringSupplementaryServiceType boiexh;
    protected GsmHlrCfCfuCfbCfnrcSupplementaryServiceType cfu;
    protected GsmHlrCfCfuCfbCfnrcSupplementaryServiceType cfb;
    protected GsmHlrCfCfuCfbCfnrcSupplementaryServiceType cfnrc;
    protected GsmHlrCfCfnryDcfSupplementaryServiceType cfnry;
    protected GsmHlrCfCfnryDcfSupplementaryServiceType dcf;
    protected GsmHlrCfSpnSupplementaryServiceType spn;
    protected GsmHlrCawSupplementaryServiceType caw;
    protected Integer acc;
    protected Integer aoc;
    protected Integer bs21;
    protected Integer bs22;
    protected Integer bs23;
    protected Integer bs24;
    protected Integer bs25;
    protected Integer bs26;
    @XmlElement(name = "bs2f")
    protected Integer bs2F;
    @XmlElement(name = "bs2g")
    protected Integer bs2G;
    protected Integer bs31;
    protected Integer bs32;
    protected Integer bs33;
    protected Integer bs34;
    @XmlElement(name = "bs3f")
    protected Integer bs3F;
    @XmlElement(name = "bs3g")
    protected Integer bs3G;
    protected Integer capl;
    protected Integer cat;
    protected Integer clip;
    protected Integer clir;
    protected Integer colp;
    protected Integer colr;
    protected Integer dbsg;
    protected Integer hold;
    protected Integer hpn;
    protected Integer ici;
    protected Integer mpty;
    protected Integer oba;
    protected Integer obi;
    protected Integer obo;
    protected Integer obopre;
    protected Integer obopri;
    protected Integer obrf;
    protected Integer obssm;
    protected Integer obr;
    protected Integer obzi;
    protected Integer obzo;
    protected Integer oin;
    protected Integer oick;
    protected Integer osb1;
    protected Integer osb2;
    protected Integer osb3;
    protected Integer osb4;
    protected Integer ofa;
    protected Integer pai;
    protected Integer regser;
    protected String pici;
    protected String pici2;
    protected String pici3;
    protected String pwd;
    protected Integer socb;
    protected Integer socfb;
    protected Integer socfrc;
    protected Integer socfry;
    protected Integer socfu;
    protected Integer soclip;
    protected Integer soclir;
    protected Integer socolp;
    protected Integer sodcf;
    protected Integer soplcs;
    protected Integer sosdcf;
    protected Integer stype;
    protected Integer tick;
    protected Integer tin;
    protected Integer ts11;
    protected Integer ts21;
    protected Integer ts22;
    protected Integer ts61;
    protected Integer ts62;
    protected GsmHlrLocationDataTypeType locationData;
    protected String vlrData;
    protected Integer emlpp;
    protected Integer demlpp;
    protected Integer memlpp;
    protected Integer tsmo;
    protected Integer sendMemlppFirst;
    protected Integer rsa;
    protected Integer obp;
    protected Integer ect;
    protected Integer obct;
    protected Integer obdct;
    protected Integer obmct;
    protected String schar;
    protected Integer ocsi;
    protected Integer tcsi;
    protected Integer gprcsi;
    protected Integer osmcsi;
    protected Integer vtcsi;
    protected Integer mcsi;
    protected Integer dcsi;
    protected Integer tsmcsi;
    protected Integer mmint;
    protected Integer cug;
    protected Integer tsd1;
    protected Integer red;
    protected Integer asl;
    protected Integer bsl;
    protected Integer crel;
    protected Integer cunrl;
    protected Integer plmno;
    protected Integer ttp;
    protected Integer univ;
    protected Integer servtl;
    protected Integer ste;
    protected Integer teardown;
    protected List<GsmHlrGmlcAddTypeType> gmlca;
    protected GsmHlrGmlcAddTypeExtType gmlca12;
    protected GsmHlrLcsDataTypeType lcsd;
    protected GetResponseGsmHlrSteTypeType steMo;
    protected Integer prbt;
    protected Integer dualstat;
    protected Integer submch;
    protected String dualstatnumber;
    protected Integer acr;
    protected Integer rtca;
    protected Integer redmch;
    protected Integer mca;
    protected Integer smspam;
    protected Integer ist;
    protected Integer istcso;
    protected Integer istgso;
    protected Integer istvso;
    protected Integer msim;
    protected GsmHlrMultiSimTypeType multiSim;
    protected GsmHlrSmsSpamTypeType smsSpam;
    protected Integer ard;
    protected String imeisv;
    protected Integer smshr1;
    protected Integer mderbt;
    @XmlAttribute(name = "msisdn")
    protected String msisdnAttr;
    @XmlAttribute(name = "imsi")
    protected String imsiAttr;

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the imsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * Sets the value of the imsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsi(String value) {
        this.imsi = value;
    }

    /**
     * Gets the value of the neDestAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNeDestAddress() {
        return neDestAddress;
    }

    /**
     * Sets the value of the neDestAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNeDestAddress(String value) {
        this.neDestAddress = value;
    }

    /**
     * Gets the value of the authd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthd() {
        return authd;
    }

    /**
     * Sets the value of the authd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthd(String value) {
        this.authd = value;
    }

    /**
     * Gets the value of the profileId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProfileId() {
        return profileId;
    }

    /**
     * Sets the value of the profileId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProfileId(Integer value) {
        this.profileId = value;
    }

    /**
     * Gets the value of the pdpcp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPdpcp() {
        return pdpcp;
    }

    /**
     * Sets the value of the pdpcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPdpcp(Integer value) {
        this.pdpcp = value;
    }

    /**
     * Gets the value of the csp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCsp() {
        return csp;
    }

    /**
     * Sets the value of the csp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCsp(Integer value) {
        this.csp = value;
    }

    /**
     * Gets the value of the amsisdn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the amsisdn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAmsisdn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrAmsisdnTypeType }
     * 
     * 
     */
    public List<GsmHlrAmsisdnTypeType> getAmsisdn() {
        if (amsisdn == null) {
            amsisdn = new ArrayList<GsmHlrAmsisdnTypeType>();
        }
        return this.amsisdn;
    }

    /**
     * Gets the value of the camel property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCamelTypeType }
     *     
     */
    public GsmHlrCamelTypeType getCamel() {
        return camel;
    }

    /**
     * Sets the value of the camel property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCamelTypeType }
     *     
     */
    public void setCamel(GsmHlrCamelTypeType value) {
        this.camel = value;
    }

    /**
     * Gets the value of the eoCamel property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrEOCamelTypeType }
     *     
     */
    public GsmHlrEOCamelTypeType getEoCamel() {
        return eoCamel;
    }

    /**
     * Sets the value of the eoCamel property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrEOCamelTypeType }
     *     
     */
    public void setEoCamel(GsmHlrEOCamelTypeType value) {
        this.eoCamel = value;
    }

    /**
     * Gets the value of the mmintMo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mmintMo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMmintMo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrMobManINTriggeringTypeType }
     * 
     * 
     */
    public List<GsmHlrMobManINTriggeringTypeType> getMmintMo() {
        if (mmintMo == null) {
            mmintMo = new ArrayList<GsmHlrMobManINTriggeringTypeType>();
        }
        return this.mmintMo;
    }

    /**
     * Gets the value of the closedUserGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the closedUserGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClosedUserGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrClosedUserGroupTypeType }
     * 
     * 
     */
    public List<GsmHlrClosedUserGroupTypeType> getClosedUserGroup() {
        if (closedUserGroup == null) {
            closedUserGroup = new ArrayList<GsmHlrClosedUserGroupTypeType>();
        }
        return this.closedUserGroup;
    }

    /**
     * Gets the value of the cugBsgOption property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCugBsgOptionTypeType }
     *     
     */
    public GsmHlrCugBsgOptionTypeType getCugBsgOption() {
        return cugBsgOption;
    }

    /**
     * Sets the value of the cugBsgOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCugBsgOptionTypeType }
     *     
     */
    public void setCugBsgOption(GsmHlrCugBsgOptionTypeType value) {
        this.cugBsgOption = value;
    }

    /**
     * Gets the value of the removeReferences property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRemoveReferences() {
        return removeReferences;
    }

    /**
     * Sets the value of the removeReferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRemoveReferences(Boolean value) {
        this.removeReferences = value;
    }

    /**
     * Gets the value of the gprs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gprs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGprs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrGprsTypeType }
     * 
     * 
     */
    public List<GsmHlrGprsTypeType> getGprs() {
        if (gprs == null) {
            gprs = new ArrayList<GsmHlrGprsTypeType>();
        }
        return this.gprs;
    }

    /**
     * Gets the value of the nam property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrNamTypeType }
     *     
     */
    public GsmHlrNamTypeType getNam() {
        return nam;
    }

    /**
     * Sets the value of the nam property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrNamTypeType }
     *     
     */
    public void setNam(GsmHlrNamTypeType value) {
        this.nam = value;
    }

    /**
     * Gets the value of the baic property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public GsmHlrBarringSupplementaryServiceType getBaic() {
        return baic;
    }

    /**
     * Sets the value of the baic property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public void setBaic(GsmHlrBarringSupplementaryServiceType value) {
        this.baic = value;
    }

    /**
     * Gets the value of the baoc property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public GsmHlrBarringSupplementaryServiceType getBaoc() {
        return baoc;
    }

    /**
     * Sets the value of the baoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public void setBaoc(GsmHlrBarringSupplementaryServiceType value) {
        this.baoc = value;
    }

    /**
     * Gets the value of the boic property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public GsmHlrBarringSupplementaryServiceType getBoic() {
        return boic;
    }

    /**
     * Sets the value of the boic property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public void setBoic(GsmHlrBarringSupplementaryServiceType value) {
        this.boic = value;
    }

    /**
     * Gets the value of the bicro property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public GsmHlrBarringSupplementaryServiceType getBicro() {
        return bicro;
    }

    /**
     * Sets the value of the bicro property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public void setBicro(GsmHlrBarringSupplementaryServiceType value) {
        this.bicro = value;
    }

    /**
     * Gets the value of the boiexh property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public GsmHlrBarringSupplementaryServiceType getBoiexh() {
        return boiexh;
    }

    /**
     * Sets the value of the boiexh property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrBarringSupplementaryServiceType }
     *     
     */
    public void setBoiexh(GsmHlrBarringSupplementaryServiceType value) {
        this.boiexh = value;
    }

    /**
     * Gets the value of the cfu property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCfCfuCfbCfnrcSupplementaryServiceType }
     *     
     */
    public GsmHlrCfCfuCfbCfnrcSupplementaryServiceType getCfu() {
        return cfu;
    }

    /**
     * Sets the value of the cfu property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCfCfuCfbCfnrcSupplementaryServiceType }
     *     
     */
    public void setCfu(GsmHlrCfCfuCfbCfnrcSupplementaryServiceType value) {
        this.cfu = value;
    }

    /**
     * Gets the value of the cfb property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCfCfuCfbCfnrcSupplementaryServiceType }
     *     
     */
    public GsmHlrCfCfuCfbCfnrcSupplementaryServiceType getCfb() {
        return cfb;
    }

    /**
     * Sets the value of the cfb property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCfCfuCfbCfnrcSupplementaryServiceType }
     *     
     */
    public void setCfb(GsmHlrCfCfuCfbCfnrcSupplementaryServiceType value) {
        this.cfb = value;
    }

    /**
     * Gets the value of the cfnrc property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCfCfuCfbCfnrcSupplementaryServiceType }
     *     
     */
    public GsmHlrCfCfuCfbCfnrcSupplementaryServiceType getCfnrc() {
        return cfnrc;
    }

    /**
     * Sets the value of the cfnrc property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCfCfuCfbCfnrcSupplementaryServiceType }
     *     
     */
    public void setCfnrc(GsmHlrCfCfuCfbCfnrcSupplementaryServiceType value) {
        this.cfnrc = value;
    }

    /**
     * Gets the value of the cfnry property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCfCfnryDcfSupplementaryServiceType }
     *     
     */
    public GsmHlrCfCfnryDcfSupplementaryServiceType getCfnry() {
        return cfnry;
    }

    /**
     * Sets the value of the cfnry property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCfCfnryDcfSupplementaryServiceType }
     *     
     */
    public void setCfnry(GsmHlrCfCfnryDcfSupplementaryServiceType value) {
        this.cfnry = value;
    }

    /**
     * Gets the value of the dcf property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCfCfnryDcfSupplementaryServiceType }
     *     
     */
    public GsmHlrCfCfnryDcfSupplementaryServiceType getDcf() {
        return dcf;
    }

    /**
     * Sets the value of the dcf property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCfCfnryDcfSupplementaryServiceType }
     *     
     */
    public void setDcf(GsmHlrCfCfnryDcfSupplementaryServiceType value) {
        this.dcf = value;
    }

    /**
     * Gets the value of the spn property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCfSpnSupplementaryServiceType }
     *     
     */
    public GsmHlrCfSpnSupplementaryServiceType getSpn() {
        return spn;
    }

    /**
     * Sets the value of the spn property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCfSpnSupplementaryServiceType }
     *     
     */
    public void setSpn(GsmHlrCfSpnSupplementaryServiceType value) {
        this.spn = value;
    }

    /**
     * Gets the value of the caw property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrCawSupplementaryServiceType }
     *     
     */
    public GsmHlrCawSupplementaryServiceType getCaw() {
        return caw;
    }

    /**
     * Sets the value of the caw property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrCawSupplementaryServiceType }
     *     
     */
    public void setCaw(GsmHlrCawSupplementaryServiceType value) {
        this.caw = value;
    }

    /**
     * Gets the value of the acc property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAcc() {
        return acc;
    }

    /**
     * Sets the value of the acc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAcc(Integer value) {
        this.acc = value;
    }

    /**
     * Gets the value of the aoc property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAoc() {
        return aoc;
    }

    /**
     * Sets the value of the aoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAoc(Integer value) {
        this.aoc = value;
    }

    /**
     * Gets the value of the bs21 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs21() {
        return bs21;
    }

    /**
     * Sets the value of the bs21 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs21(Integer value) {
        this.bs21 = value;
    }

    /**
     * Gets the value of the bs22 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs22() {
        return bs22;
    }

    /**
     * Sets the value of the bs22 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs22(Integer value) {
        this.bs22 = value;
    }

    /**
     * Gets the value of the bs23 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs23() {
        return bs23;
    }

    /**
     * Sets the value of the bs23 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs23(Integer value) {
        this.bs23 = value;
    }

    /**
     * Gets the value of the bs24 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs24() {
        return bs24;
    }

    /**
     * Sets the value of the bs24 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs24(Integer value) {
        this.bs24 = value;
    }

    /**
     * Gets the value of the bs25 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs25() {
        return bs25;
    }

    /**
     * Sets the value of the bs25 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs25(Integer value) {
        this.bs25 = value;
    }

    /**
     * Gets the value of the bs26 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs26() {
        return bs26;
    }

    /**
     * Sets the value of the bs26 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs26(Integer value) {
        this.bs26 = value;
    }

    /**
     * Gets the value of the bs2F property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs2F() {
        return bs2F;
    }

    /**
     * Sets the value of the bs2F property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs2F(Integer value) {
        this.bs2F = value;
    }

    /**
     * Gets the value of the bs2G property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs2G() {
        return bs2G;
    }

    /**
     * Sets the value of the bs2G property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs2G(Integer value) {
        this.bs2G = value;
    }

    /**
     * Gets the value of the bs31 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs31() {
        return bs31;
    }

    /**
     * Sets the value of the bs31 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs31(Integer value) {
        this.bs31 = value;
    }

    /**
     * Gets the value of the bs32 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs32() {
        return bs32;
    }

    /**
     * Sets the value of the bs32 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs32(Integer value) {
        this.bs32 = value;
    }

    /**
     * Gets the value of the bs33 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs33() {
        return bs33;
    }

    /**
     * Sets the value of the bs33 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs33(Integer value) {
        this.bs33 = value;
    }

    /**
     * Gets the value of the bs34 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs34() {
        return bs34;
    }

    /**
     * Sets the value of the bs34 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs34(Integer value) {
        this.bs34 = value;
    }

    /**
     * Gets the value of the bs3F property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs3F() {
        return bs3F;
    }

    /**
     * Sets the value of the bs3F property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs3F(Integer value) {
        this.bs3F = value;
    }

    /**
     * Gets the value of the bs3G property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBs3G() {
        return bs3G;
    }

    /**
     * Sets the value of the bs3G property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBs3G(Integer value) {
        this.bs3G = value;
    }

    /**
     * Gets the value of the capl property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapl() {
        return capl;
    }

    /**
     * Sets the value of the capl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapl(Integer value) {
        this.capl = value;
    }

    /**
     * Gets the value of the cat property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCat() {
        return cat;
    }

    /**
     * Sets the value of the cat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCat(Integer value) {
        this.cat = value;
    }

    /**
     * Gets the value of the clip property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClip() {
        return clip;
    }

    /**
     * Sets the value of the clip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClip(Integer value) {
        this.clip = value;
    }

    /**
     * Gets the value of the clir property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClir() {
        return clir;
    }

    /**
     * Sets the value of the clir property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClir(Integer value) {
        this.clir = value;
    }

    /**
     * Gets the value of the colp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getColp() {
        return colp;
    }

    /**
     * Sets the value of the colp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setColp(Integer value) {
        this.colp = value;
    }

    /**
     * Gets the value of the colr property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getColr() {
        return colr;
    }

    /**
     * Sets the value of the colr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setColr(Integer value) {
        this.colr = value;
    }

    /**
     * Gets the value of the dbsg property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDbsg() {
        return dbsg;
    }

    /**
     * Sets the value of the dbsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDbsg(Integer value) {
        this.dbsg = value;
    }

    /**
     * Gets the value of the hold property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHold() {
        return hold;
    }

    /**
     * Sets the value of the hold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHold(Integer value) {
        this.hold = value;
    }

    /**
     * Gets the value of the hpn property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHpn() {
        return hpn;
    }

    /**
     * Sets the value of the hpn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHpn(Integer value) {
        this.hpn = value;
    }

    /**
     * Gets the value of the ici property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIci() {
        return ici;
    }

    /**
     * Sets the value of the ici property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIci(Integer value) {
        this.ici = value;
    }

    /**
     * Gets the value of the mpty property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMpty() {
        return mpty;
    }

    /**
     * Sets the value of the mpty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMpty(Integer value) {
        this.mpty = value;
    }

    /**
     * Gets the value of the oba property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOba() {
        return oba;
    }

    /**
     * Sets the value of the oba property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOba(Integer value) {
        this.oba = value;
    }

    /**
     * Gets the value of the obi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObi() {
        return obi;
    }

    /**
     * Sets the value of the obi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObi(Integer value) {
        this.obi = value;
    }

    /**
     * Gets the value of the obo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObo() {
        return obo;
    }

    /**
     * Sets the value of the obo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObo(Integer value) {
        this.obo = value;
    }

    /**
     * Gets the value of the obopre property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObopre() {
        return obopre;
    }

    /**
     * Sets the value of the obopre property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObopre(Integer value) {
        this.obopre = value;
    }

    /**
     * Gets the value of the obopri property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObopri() {
        return obopri;
    }

    /**
     * Sets the value of the obopri property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObopri(Integer value) {
        this.obopri = value;
    }

    /**
     * Gets the value of the obrf property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObrf() {
        return obrf;
    }

    /**
     * Sets the value of the obrf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObrf(Integer value) {
        this.obrf = value;
    }

    /**
     * Gets the value of the obssm property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObssm() {
        return obssm;
    }

    /**
     * Sets the value of the obssm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObssm(Integer value) {
        this.obssm = value;
    }

    /**
     * Gets the value of the obr property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObr() {
        return obr;
    }

    /**
     * Sets the value of the obr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObr(Integer value) {
        this.obr = value;
    }

    /**
     * Gets the value of the obzi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObzi() {
        return obzi;
    }

    /**
     * Sets the value of the obzi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObzi(Integer value) {
        this.obzi = value;
    }

    /**
     * Gets the value of the obzo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObzo() {
        return obzo;
    }

    /**
     * Sets the value of the obzo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObzo(Integer value) {
        this.obzo = value;
    }

    /**
     * Gets the value of the oin property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOin() {
        return oin;
    }

    /**
     * Sets the value of the oin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOin(Integer value) {
        this.oin = value;
    }

    /**
     * Gets the value of the oick property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOick() {
        return oick;
    }

    /**
     * Sets the value of the oick property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOick(Integer value) {
        this.oick = value;
    }

    /**
     * Gets the value of the osb1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOsb1() {
        return osb1;
    }

    /**
     * Sets the value of the osb1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOsb1(Integer value) {
        this.osb1 = value;
    }

    /**
     * Gets the value of the osb2 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOsb2() {
        return osb2;
    }

    /**
     * Sets the value of the osb2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOsb2(Integer value) {
        this.osb2 = value;
    }

    /**
     * Gets the value of the osb3 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOsb3() {
        return osb3;
    }

    /**
     * Sets the value of the osb3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOsb3(Integer value) {
        this.osb3 = value;
    }

    /**
     * Gets the value of the osb4 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOsb4() {
        return osb4;
    }

    /**
     * Sets the value of the osb4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOsb4(Integer value) {
        this.osb4 = value;
    }

    /**
     * Gets the value of the ofa property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOfa() {
        return ofa;
    }

    /**
     * Sets the value of the ofa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOfa(Integer value) {
        this.ofa = value;
    }

    /**
     * Gets the value of the pai property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPai() {
        return pai;
    }

    /**
     * Sets the value of the pai property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPai(Integer value) {
        this.pai = value;
    }

    /**
     * Gets the value of the regser property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRegser() {
        return regser;
    }

    /**
     * Sets the value of the regser property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRegser(Integer value) {
        this.regser = value;
    }

    /**
     * Gets the value of the pici property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPici() {
        return pici;
    }

    /**
     * Sets the value of the pici property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPici(String value) {
        this.pici = value;
    }

    /**
     * Gets the value of the pici2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPici2() {
        return pici2;
    }

    /**
     * Sets the value of the pici2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPici2(String value) {
        this.pici2 = value;
    }

    /**
     * Gets the value of the pici3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPici3() {
        return pici3;
    }

    /**
     * Sets the value of the pici3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPici3(String value) {
        this.pici3 = value;
    }

    /**
     * Gets the value of the pwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * Sets the value of the pwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPwd(String value) {
        this.pwd = value;
    }

    /**
     * Gets the value of the socb property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSocb() {
        return socb;
    }

    /**
     * Sets the value of the socb property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSocb(Integer value) {
        this.socb = value;
    }

    /**
     * Gets the value of the socfb property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSocfb() {
        return socfb;
    }

    /**
     * Sets the value of the socfb property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSocfb(Integer value) {
        this.socfb = value;
    }

    /**
     * Gets the value of the socfrc property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSocfrc() {
        return socfrc;
    }

    /**
     * Sets the value of the socfrc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSocfrc(Integer value) {
        this.socfrc = value;
    }

    /**
     * Gets the value of the socfry property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSocfry() {
        return socfry;
    }

    /**
     * Sets the value of the socfry property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSocfry(Integer value) {
        this.socfry = value;
    }

    /**
     * Gets the value of the socfu property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSocfu() {
        return socfu;
    }

    /**
     * Sets the value of the socfu property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSocfu(Integer value) {
        this.socfu = value;
    }

    /**
     * Gets the value of the soclip property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSoclip() {
        return soclip;
    }

    /**
     * Sets the value of the soclip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSoclip(Integer value) {
        this.soclip = value;
    }

    /**
     * Gets the value of the soclir property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSoclir() {
        return soclir;
    }

    /**
     * Sets the value of the soclir property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSoclir(Integer value) {
        this.soclir = value;
    }

    /**
     * Gets the value of the socolp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSocolp() {
        return socolp;
    }

    /**
     * Sets the value of the socolp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSocolp(Integer value) {
        this.socolp = value;
    }

    /**
     * Gets the value of the sodcf property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSodcf() {
        return sodcf;
    }

    /**
     * Sets the value of the sodcf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSodcf(Integer value) {
        this.sodcf = value;
    }

    /**
     * Gets the value of the soplcs property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSoplcs() {
        return soplcs;
    }

    /**
     * Sets the value of the soplcs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSoplcs(Integer value) {
        this.soplcs = value;
    }

    /**
     * Gets the value of the sosdcf property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSosdcf() {
        return sosdcf;
    }

    /**
     * Sets the value of the sosdcf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSosdcf(Integer value) {
        this.sosdcf = value;
    }

    /**
     * Gets the value of the stype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStype() {
        return stype;
    }

    /**
     * Sets the value of the stype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStype(Integer value) {
        this.stype = value;
    }

    /**
     * Gets the value of the tick property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTick() {
        return tick;
    }

    /**
     * Sets the value of the tick property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTick(Integer value) {
        this.tick = value;
    }

    /**
     * Gets the value of the tin property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTin() {
        return tin;
    }

    /**
     * Sets the value of the tin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTin(Integer value) {
        this.tin = value;
    }

    /**
     * Gets the value of the ts11 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTs11() {
        return ts11;
    }

    /**
     * Sets the value of the ts11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTs11(Integer value) {
        this.ts11 = value;
    }

    /**
     * Gets the value of the ts21 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTs21() {
        return ts21;
    }

    /**
     * Sets the value of the ts21 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTs21(Integer value) {
        this.ts21 = value;
    }

    /**
     * Gets the value of the ts22 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTs22() {
        return ts22;
    }

    /**
     * Sets the value of the ts22 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTs22(Integer value) {
        this.ts22 = value;
    }

    /**
     * Gets the value of the ts61 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTs61() {
        return ts61;
    }

    /**
     * Sets the value of the ts61 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTs61(Integer value) {
        this.ts61 = value;
    }

    /**
     * Gets the value of the ts62 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTs62() {
        return ts62;
    }

    /**
     * Sets the value of the ts62 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTs62(Integer value) {
        this.ts62 = value;
    }

    /**
     * Gets the value of the locationData property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrLocationDataTypeType }
     *     
     */
    public GsmHlrLocationDataTypeType getLocationData() {
        return locationData;
    }

    /**
     * Sets the value of the locationData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrLocationDataTypeType }
     *     
     */
    public void setLocationData(GsmHlrLocationDataTypeType value) {
        this.locationData = value;
    }

    /**
     * Gets the value of the vlrData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVlrData() {
        return vlrData;
    }

    /**
     * Sets the value of the vlrData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVlrData(String value) {
        this.vlrData = value;
    }

    /**
     * Gets the value of the emlpp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmlpp() {
        return emlpp;
    }

    /**
     * Sets the value of the emlpp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmlpp(Integer value) {
        this.emlpp = value;
    }

    /**
     * Gets the value of the demlpp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemlpp() {
        return demlpp;
    }

    /**
     * Sets the value of the demlpp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemlpp(Integer value) {
        this.demlpp = value;
    }

    /**
     * Gets the value of the memlpp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMemlpp() {
        return memlpp;
    }

    /**
     * Sets the value of the memlpp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMemlpp(Integer value) {
        this.memlpp = value;
    }

    /**
     * Gets the value of the tsmo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTsmo() {
        return tsmo;
    }

    /**
     * Sets the value of the tsmo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTsmo(Integer value) {
        this.tsmo = value;
    }

    /**
     * Gets the value of the sendMemlppFirst property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSendMemlppFirst() {
        return sendMemlppFirst;
    }

    /**
     * Sets the value of the sendMemlppFirst property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSendMemlppFirst(Integer value) {
        this.sendMemlppFirst = value;
    }

    /**
     * Gets the value of the rsa property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRsa() {
        return rsa;
    }

    /**
     * Sets the value of the rsa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRsa(Integer value) {
        this.rsa = value;
    }

    /**
     * Gets the value of the obp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObp() {
        return obp;
    }

    /**
     * Sets the value of the obp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObp(Integer value) {
        this.obp = value;
    }

    /**
     * Gets the value of the ect property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEct() {
        return ect;
    }

    /**
     * Sets the value of the ect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEct(Integer value) {
        this.ect = value;
    }

    /**
     * Gets the value of the obct property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObct() {
        return obct;
    }

    /**
     * Sets the value of the obct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObct(Integer value) {
        this.obct = value;
    }

    /**
     * Gets the value of the obdct property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObdct() {
        return obdct;
    }

    /**
     * Sets the value of the obdct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObdct(Integer value) {
        this.obdct = value;
    }

    /**
     * Gets the value of the obmct property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObmct() {
        return obmct;
    }

    /**
     * Sets the value of the obmct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObmct(Integer value) {
        this.obmct = value;
    }

    /**
     * Gets the value of the schar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchar() {
        return schar;
    }

    /**
     * Sets the value of the schar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchar(String value) {
        this.schar = value;
    }

    /**
     * Gets the value of the ocsi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOcsi() {
        return ocsi;
    }

    /**
     * Sets the value of the ocsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOcsi(Integer value) {
        this.ocsi = value;
    }

    /**
     * Gets the value of the tcsi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTcsi() {
        return tcsi;
    }

    /**
     * Sets the value of the tcsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTcsi(Integer value) {
        this.tcsi = value;
    }

    /**
     * Gets the value of the gprcsi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGprcsi() {
        return gprcsi;
    }

    /**
     * Sets the value of the gprcsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGprcsi(Integer value) {
        this.gprcsi = value;
    }

    /**
     * Gets the value of the osmcsi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOsmcsi() {
        return osmcsi;
    }

    /**
     * Sets the value of the osmcsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOsmcsi(Integer value) {
        this.osmcsi = value;
    }

    /**
     * Gets the value of the vtcsi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVtcsi() {
        return vtcsi;
    }

    /**
     * Sets the value of the vtcsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVtcsi(Integer value) {
        this.vtcsi = value;
    }

    /**
     * Gets the value of the mcsi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMcsi() {
        return mcsi;
    }

    /**
     * Sets the value of the mcsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMcsi(Integer value) {
        this.mcsi = value;
    }

    /**
     * Gets the value of the dcsi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDcsi() {
        return dcsi;
    }

    /**
     * Sets the value of the dcsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDcsi(Integer value) {
        this.dcsi = value;
    }

    /**
     * Gets the value of the tsmcsi property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTsmcsi() {
        return tsmcsi;
    }

    /**
     * Sets the value of the tsmcsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTsmcsi(Integer value) {
        this.tsmcsi = value;
    }

    /**
     * Gets the value of the mmint property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMmint() {
        return mmint;
    }

    /**
     * Sets the value of the mmint property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMmint(Integer value) {
        this.mmint = value;
    }

    /**
     * Gets the value of the cug property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCug() {
        return cug;
    }

    /**
     * Sets the value of the cug property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCug(Integer value) {
        this.cug = value;
    }

    /**
     * Gets the value of the tsd1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTsd1() {
        return tsd1;
    }

    /**
     * Sets the value of the tsd1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTsd1(Integer value) {
        this.tsd1 = value;
    }

    /**
     * Gets the value of the red property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRed() {
        return red;
    }

    /**
     * Sets the value of the red property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRed(Integer value) {
        this.red = value;
    }

    /**
     * Gets the value of the asl property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAsl() {
        return asl;
    }

    /**
     * Sets the value of the asl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAsl(Integer value) {
        this.asl = value;
    }

    /**
     * Gets the value of the bsl property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBsl() {
        return bsl;
    }

    /**
     * Sets the value of the bsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBsl(Integer value) {
        this.bsl = value;
    }

    /**
     * Gets the value of the crel property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCrel() {
        return crel;
    }

    /**
     * Sets the value of the crel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCrel(Integer value) {
        this.crel = value;
    }

    /**
     * Gets the value of the cunrl property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCunrl() {
        return cunrl;
    }

    /**
     * Sets the value of the cunrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCunrl(Integer value) {
        this.cunrl = value;
    }

    /**
     * Gets the value of the plmno property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlmno() {
        return plmno;
    }

    /**
     * Sets the value of the plmno property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlmno(Integer value) {
        this.plmno = value;
    }

    /**
     * Gets the value of the ttp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTtp() {
        return ttp;
    }

    /**
     * Sets the value of the ttp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTtp(Integer value) {
        this.ttp = value;
    }

    /**
     * Gets the value of the univ property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUniv() {
        return univ;
    }

    /**
     * Sets the value of the univ property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUniv(Integer value) {
        this.univ = value;
    }

    /**
     * Gets the value of the servtl property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServtl() {
        return servtl;
    }

    /**
     * Sets the value of the servtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServtl(Integer value) {
        this.servtl = value;
    }

    /**
     * Gets the value of the ste property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSte() {
        return ste;
    }

    /**
     * Sets the value of the ste property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSte(Integer value) {
        this.ste = value;
    }

    /**
     * Gets the value of the teardown property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTeardown() {
        return teardown;
    }

    /**
     * Sets the value of the teardown property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTeardown(Integer value) {
        this.teardown = value;
    }

    /**
     * Gets the value of the gmlca property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gmlca property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGmlca().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrGmlcAddTypeType }
     * 
     * 
     */
    public List<GsmHlrGmlcAddTypeType> getGmlca() {
        if (gmlca == null) {
            gmlca = new ArrayList<GsmHlrGmlcAddTypeType>();
        }
        return this.gmlca;
    }

    /**
     * Gets the value of the gmlca12 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrGmlcAddTypeExtType }
     *     
     */
    public GsmHlrGmlcAddTypeExtType getGmlca12() {
        return gmlca12;
    }

    /**
     * Sets the value of the gmlca12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrGmlcAddTypeExtType }
     *     
     */
    public void setGmlca12(GsmHlrGmlcAddTypeExtType value) {
        this.gmlca12 = value;
    }

    /**
     * Gets the value of the lcsd property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrLcsDataTypeType }
     *     
     */
    public GsmHlrLcsDataTypeType getLcsd() {
        return lcsd;
    }

    /**
     * Sets the value of the lcsd property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrLcsDataTypeType }
     *     
     */
    public void setLcsd(GsmHlrLcsDataTypeType value) {
        this.lcsd = value;
    }

    /**
     * Gets the value of the steMo property.
     * 
     * @return
     *     possible object is
     *     {@link GetResponseGsmHlrSteTypeType }
     *     
     */
    public GetResponseGsmHlrSteTypeType getSteMo() {
        return steMo;
    }

    /**
     * Sets the value of the steMo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetResponseGsmHlrSteTypeType }
     *     
     */
    public void setSteMo(GetResponseGsmHlrSteTypeType value) {
        this.steMo = value;
    }

    /**
     * Gets the value of the prbt property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrbt() {
        return prbt;
    }

    /**
     * Sets the value of the prbt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrbt(Integer value) {
        this.prbt = value;
    }

    /**
     * Gets the value of the dualstat property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDualstat() {
        return dualstat;
    }

    /**
     * Sets the value of the dualstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDualstat(Integer value) {
        this.dualstat = value;
    }

    /**
     * Gets the value of the submch property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubmch() {
        return submch;
    }

    /**
     * Sets the value of the submch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubmch(Integer value) {
        this.submch = value;
    }

    /**
     * Gets the value of the dualstatnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDualstatnumber() {
        return dualstatnumber;
    }

    /**
     * Sets the value of the dualstatnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDualstatnumber(String value) {
        this.dualstatnumber = value;
    }

    /**
     * Gets the value of the acr property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAcr() {
        return acr;
    }

    /**
     * Sets the value of the acr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAcr(Integer value) {
        this.acr = value;
    }

    /**
     * Gets the value of the rtca property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRtca() {
        return rtca;
    }

    /**
     * Sets the value of the rtca property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRtca(Integer value) {
        this.rtca = value;
    }

    /**
     * Gets the value of the redmch property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRedmch() {
        return redmch;
    }

    /**
     * Sets the value of the redmch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRedmch(Integer value) {
        this.redmch = value;
    }

    /**
     * Gets the value of the mca property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMca() {
        return mca;
    }

    /**
     * Sets the value of the mca property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMca(Integer value) {
        this.mca = value;
    }

    /**
     * Gets the value of the smspam property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSmspam() {
        return smspam;
    }

    /**
     * Sets the value of the smspam property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSmspam(Integer value) {
        this.smspam = value;
    }

    /**
     * Gets the value of the ist property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIst() {
        return ist;
    }

    /**
     * Sets the value of the ist property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIst(Integer value) {
        this.ist = value;
    }

    /**
     * Gets the value of the istcso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIstcso() {
        return istcso;
    }

    /**
     * Sets the value of the istcso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIstcso(Integer value) {
        this.istcso = value;
    }

    /**
     * Gets the value of the istgso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIstgso() {
        return istgso;
    }

    /**
     * Sets the value of the istgso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIstgso(Integer value) {
        this.istgso = value;
    }

    /**
     * Gets the value of the istvso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIstvso() {
        return istvso;
    }

    /**
     * Sets the value of the istvso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIstvso(Integer value) {
        this.istvso = value;
    }

    /**
     * Gets the value of the msim property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMsim() {
        return msim;
    }

    /**
     * Sets the value of the msim property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMsim(Integer value) {
        this.msim = value;
    }

    /**
     * Gets the value of the multiSim property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrMultiSimTypeType }
     *     
     */
    public GsmHlrMultiSimTypeType getMultiSim() {
        return multiSim;
    }

    /**
     * Sets the value of the multiSim property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrMultiSimTypeType }
     *     
     */
    public void setMultiSim(GsmHlrMultiSimTypeType value) {
        this.multiSim = value;
    }

    /**
     * Gets the value of the smsSpam property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrSmsSpamTypeType }
     *     
     */
    public GsmHlrSmsSpamTypeType getSmsSpam() {
        return smsSpam;
    }

    /**
     * Sets the value of the smsSpam property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrSmsSpamTypeType }
     *     
     */
    public void setSmsSpam(GsmHlrSmsSpamTypeType value) {
        this.smsSpam = value;
    }

    /**
     * Gets the value of the ard property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getArd() {
        return ard;
    }

    /**
     * Sets the value of the ard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setArd(Integer value) {
        this.ard = value;
    }

    /**
     * Gets the value of the imeisv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImeisv() {
        return imeisv;
    }

    /**
     * Sets the value of the imeisv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImeisv(String value) {
        this.imeisv = value;
    }

    /**
     * Gets the value of the smshr1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSmshr1() {
        return smshr1;
    }

    /**
     * Sets the value of the smshr1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSmshr1(Integer value) {
        this.smshr1 = value;
    }

    /**
     * Gets the value of the mderbt property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMderbt() {
        return mderbt;
    }

    /**
     * Sets the value of the mderbt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMderbt(Integer value) {
        this.mderbt = value;
    }

    /**
     * Gets the value of the msisdnAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnAttr() {
        return msisdnAttr;
    }

    /**
     * Sets the value of the msisdnAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnAttr(String value) {
        this.msisdnAttr = value;
    }

    /**
     * Gets the value of the imsiAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsiAttr() {
        return imsiAttr;
    }

    /**
     * Sets the value of the imsiAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsiAttr(String value) {
        this.imsiAttr = value;
    }

}
