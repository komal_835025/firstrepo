package com.celcom.ms.ema.session.adp.soapclient;

import java.math.BigInteger;

import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.celcom.ms.ema.session.adp.model.EMASessionControlResDTO;
import com.celcom.ms.ema.session.adp.soap.beans.cai3g1.CAI3G;
import com.celcom.ms.ema.session.adp.soap.beans.cai3g1.Cai3GFault;
import com.celcom.ms.ema.session.adp.soap.beans.cai3g1.SessionControl;
import com.celcom.ms.ema.session.adp.util.GetProperties;
import com.celcom.ms.ema.session.adp.util.Properties;

@Component
public class EMASessionControlAdpSOAPClient {

	private static final Logger logger = LoggerFactory.getLogger(EMASessionControlAdpSOAPClient.class);

	@Autowired
	Environment environment;

	private EMASessionControlResDTO resDTO = null;

	public EMASessionControlResDTO invokeEMASessionControl() {
 
		logger.debug(" EMASessionControlAdpSOAPClient : executing invokeEMASessionControl() ");

		CAI3G cai3gInstance = new CAI3G();
		SessionControl sessionControl = cai3gInstance.getSessionControl();

		resDTO = new EMASessionControlResDTO();

		final Holder<String> sessionId = new Holder<String>();
		final Holder<BigInteger> sequence = new Holder<BigInteger>();

		try {
			GetProperties getProps = new GetProperties();
			System.out.println("Logging level = "+getProps.getLoggingLevel());
			System.out.println("Username = "+getProps.getUsername());
			System.out.println("Password = "+getProps.getPassword());
			logger.info("Logging level = "+getProps.getLoggingLevel());
			logger.info("Username = "+getProps.getUsername());
			logger.info("Password = "+getProps.getPassword());
			//sessionControl.login(environment.getProperty(Properties.KEY_EMA_SESSION_CONTROL_LOGIN_USERNAME),
			//		environment.getProperty(Properties.KEY_EMA_SESSION_CONTROL_LOGIN_PASSWORD), sessionId, sequence);
			sessionControl.login(getProps.getUsername(),getProps.getPassword(), sessionId, sequence);
		} catch (Cai3GFault e) {
			e.printStackTrace();
		}
		
		logger.debug(" EMASessionControlAdpSOAPClient : executing invokeEMASessionControl() : sessionId recieved : "+sessionId);
		
		resDTO.setSessionId(null == sessionId ? null : sessionId.value);
		
		if (sequence != null && sequence.value != null) {
			resDTO.setBaseSequenceId(sequence.value.toString());
		}

		return resDTO;
	}

}
