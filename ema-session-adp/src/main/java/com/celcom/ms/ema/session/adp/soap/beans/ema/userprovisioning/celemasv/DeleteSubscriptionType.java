
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeleteSubscriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeleteSubscriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType"/>
 *         &lt;element name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" minOccurs="0"/>
 *         &lt;element name="iccid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usertype" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}usertypeType"/>
 *         &lt;element name="emaprofile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodlist" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="threegind" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}ThreegindicatorType" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="optype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="opcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="msisdn" use="required" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType" />
 *       &lt;attribute name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeleteSubscriptionType", propOrder = {
    "msisdn",
    "imsi",
    "iccid",
    "usertype",
    "emaprofile",
    "prodlist",
    "threegind",
    "id",
    "optype",
    "opcode"
})
public class DeleteSubscriptionType {

    @XmlElement(required = true)
    protected String msisdn;
    protected String imsi;
    protected String iccid;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected UsertypeType usertype;
    protected String emaprofile;
    protected String prodlist;
    @XmlSchemaType(name = "string")
    protected ThreegindicatorType threegind;
    protected String id;
    protected String optype;
    protected String opcode;
    @XmlAttribute(name = "msisdn", required = true)
    protected String msisdnAttr;
    @XmlAttribute(name = "imsi")
    protected String imsiAttr;

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the imsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * Sets the value of the imsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsi(String value) {
        this.imsi = value;
    }

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIccid() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIccid(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the usertype property.
     * 
     * @return
     *     possible object is
     *     {@link UsertypeType }
     *     
     */
    public UsertypeType getUsertype() {
        return usertype;
    }

    /**
     * Sets the value of the usertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link UsertypeType }
     *     
     */
    public void setUsertype(UsertypeType value) {
        this.usertype = value;
    }

    /**
     * Gets the value of the emaprofile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmaprofile() {
        return emaprofile;
    }

    /**
     * Sets the value of the emaprofile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmaprofile(String value) {
        this.emaprofile = value;
    }

    /**
     * Gets the value of the prodlist property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdlist() {
        return prodlist;
    }

    /**
     * Sets the value of the prodlist property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdlist(String value) {
        this.prodlist = value;
    }

    /**
     * Gets the value of the threegind property.
     * 
     * @return
     *     possible object is
     *     {@link ThreegindicatorType }
     *     
     */
    public ThreegindicatorType getThreegind() {
        return threegind;
    }

    /**
     * Sets the value of the threegind property.
     * 
     * @param value
     *     allowed object is
     *     {@link ThreegindicatorType }
     *     
     */
    public void setThreegind(ThreegindicatorType value) {
        this.threegind = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the optype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptype() {
        return optype;
    }

    /**
     * Sets the value of the optype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptype(String value) {
        this.optype = value;
    }

    /**
     * Gets the value of the opcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpcode() {
        return opcode;
    }

    /**
     * Sets the value of the opcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpcode(String value) {
        this.opcode = value;
    }

    /**
     * Gets the value of the msisdnAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnAttr() {
        return msisdnAttr;
    }

    /**
     * Sets the value of the msisdnAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnAttr(String value) {
        this.msisdnAttr = value;
    }

    /**
     * Gets the value of the imsiAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsiAttr() {
        return imsiAttr;
    }

    /**
     * Sets the value of the imsiAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsiAttr(String value) {
        this.imsiAttr = value;
    }

}
