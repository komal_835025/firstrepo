
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrSmsSpamTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrSmsSpamTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="active" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="scAdds" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrSmsSpamScAddsTypeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrSmsSpamTypeType", propOrder = {
    "active",
    "scAdds"
})
public class GsmHlrSmsSpamTypeType {

    @XmlElement(required = true)
    protected String active;
    @XmlElement(required = true)
    protected List<GsmHlrSmsSpamScAddsTypeType> scAdds;

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActive(String value) {
        this.active = value;
    }

    /**
     * Gets the value of the scAdds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scAdds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScAdds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GsmHlrSmsSpamScAddsTypeType }
     * 
     * 
     */
    public List<GsmHlrSmsSpamScAddsTypeType> getScAdds() {
        if (scAdds == null) {
            scAdds = new ArrayList<GsmHlrSmsSpamScAddsTypeType>();
        }
        return this.scAdds;
    }

}
