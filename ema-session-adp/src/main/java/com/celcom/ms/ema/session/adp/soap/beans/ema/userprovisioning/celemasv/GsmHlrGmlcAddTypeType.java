
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrGmlcAddTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrGmlcAddTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="gmlcId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gmlcAddress" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="gmlcId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrGmlcAddTypeType", propOrder = {
    "gmlcId",
    "gmlcAddress"
})
public class GsmHlrGmlcAddTypeType {

    @XmlElement(required = true)
    protected String gmlcId;
    protected String gmlcAddress;
    @XmlAttribute(name = "gmlcId", required = true)
    protected String gmlcIdAttr;

    /**
     * Gets the value of the gmlcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGmlcId() {
        return gmlcId;
    }

    /**
     * Sets the value of the gmlcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGmlcId(String value) {
        this.gmlcId = value;
    }

    /**
     * Gets the value of the gmlcAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGmlcAddress() {
        return gmlcAddress;
    }

    /**
     * Sets the value of the gmlcAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGmlcAddress(String value) {
        this.gmlcAddress = value;
    }

    /**
     * Gets the value of the gmlcIdAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGmlcIdAttr() {
        return gmlcIdAttr;
    }

    /**
     * Sets the value of the gmlcIdAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGmlcIdAttr(String value) {
        this.gmlcIdAttr = value;
    }

}
