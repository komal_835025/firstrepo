
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCugSingleBsgOptionTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCugSingleBsgOptionTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accessibility" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugAccessibilityTypeType" minOccurs="0"/>
 *         &lt;element name="preferentialCug" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCugSingleBsgOptionTypeType", propOrder = {
    "accessibility",
    "preferentialCug"
})
public class GsmHlrCugSingleBsgOptionTypeType {

    protected String accessibility;
    protected String preferentialCug;

    /**
     * Gets the value of the accessibility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessibility() {
        return accessibility;
    }

    /**
     * Sets the value of the accessibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessibility(String value) {
        this.accessibility = value;
    }

    /**
     * Gets the value of the preferentialCug property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferentialCug() {
        return preferentialCug;
    }

    /**
     * Sets the value of the preferentialCug property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferentialCug(String value) {
        this.preferentialCug = value;
    }

}
