
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrSmsSpamScAddsTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrSmsSpamScAddsTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="scAdds">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="scAdds" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrSmsSpamScAddsTypeType", propOrder = {
    "scAdds"
})
public class GsmHlrSmsSpamScAddsTypeType {

    @XmlElement(required = true)
    protected String scAdds;
    @XmlAttribute(name = "scAdds", required = true)
    protected String scAddsAttr;

    /**
     * Gets the value of the scAdds property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScAdds() {
        return scAdds;
    }

    /**
     * Sets the value of the scAdds property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScAdds(String value) {
        this.scAdds = value;
    }

    /**
     * Gets the value of the scAddsAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScAddsAttr() {
        return scAddsAttr;
    }

    /**
     * Sets the value of the scAddsAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScAddsAttr(String value) {
        this.scAddsAttr = value;
    }

}
