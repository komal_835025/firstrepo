
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCfSpnSupplementaryServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCfSpnSupplementaryServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="provisionState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrProvisionTypeType"/>
 *         &lt;element name="activationState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrActivationTypeType" minOccurs="0"/>
 *         &lt;element name="fnum" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ofa" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="511"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ts10" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrFnumSpnActivationStateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCfSpnSupplementaryServiceType", propOrder = {
    "provisionState",
    "activationState",
    "fnum",
    "ofa",
    "ts10"
})
public class GsmHlrCfSpnSupplementaryServiceType {

    @XmlElement(required = true)
    protected String provisionState;
    protected String activationState;
    protected String fnum;
    protected Integer ofa;
    protected GsmHlrFnumSpnActivationStateType ts10;

    /**
     * Gets the value of the provisionState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisionState() {
        return provisionState;
    }

    /**
     * Sets the value of the provisionState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisionState(String value) {
        this.provisionState = value;
    }

    /**
     * Gets the value of the activationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationState() {
        return activationState;
    }

    /**
     * Sets the value of the activationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationState(String value) {
        this.activationState = value;
    }

    /**
     * Gets the value of the fnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnum() {
        return fnum;
    }

    /**
     * Sets the value of the fnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnum(String value) {
        this.fnum = value;
    }

    /**
     * Gets the value of the ofa property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOfa() {
        return ofa;
    }

    /**
     * Sets the value of the ofa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOfa(Integer value) {
        this.ofa = value;
    }

    /**
     * Gets the value of the ts10 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrFnumSpnActivationStateType }
     *     
     */
    public GsmHlrFnumSpnActivationStateType getTs10() {
        return ts10;
    }

    /**
     * Sets the value of the ts10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrFnumSpnActivationStateType }
     *     
     */
    public void setTs10(GsmHlrFnumSpnActivationStateType value) {
        this.ts10 = value;
    }

}
