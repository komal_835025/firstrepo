
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FnxSubscriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FnxSubscriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType" minOccurs="0"/>
 *         &lt;element name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" minOccurs="0"/>
 *         &lt;element name="nprefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subtype" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="forcedel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="norollback" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="msisdn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType" />
 *       &lt;attribute name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FnxSubscriptionType", propOrder = {
    "id",
    "msisdn",
    "imsi",
    "nprefix",
    "subtype",
    "forcedel",
    "norollback"
})
public class FnxSubscriptionType {

    protected String id;
    protected String msisdn;
    protected String imsi;
    protected String nprefix;
    protected Integer subtype;
    protected String forcedel;
    protected String norollback;
    @XmlAttribute(name = "msisdn")
    protected String msisdnAttr;
    @XmlAttribute(name = "imsi")
    protected String imsiAttr;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the imsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * Sets the value of the imsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsi(String value) {
        this.imsi = value;
    }

    /**
     * Gets the value of the nprefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNprefix() {
        return nprefix;
    }

    /**
     * Sets the value of the nprefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNprefix(String value) {
        this.nprefix = value;
    }

    /**
     * Gets the value of the subtype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubtype() {
        return subtype;
    }

    /**
     * Sets the value of the subtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubtype(Integer value) {
        this.subtype = value;
    }

    /**
     * Gets the value of the forcedel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForcedel() {
        return forcedel;
    }

    /**
     * Sets the value of the forcedel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForcedel(String value) {
        this.forcedel = value;
    }

    /**
     * Gets the value of the norollback property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNorollback() {
        return norollback;
    }

    /**
     * Sets the value of the norollback property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNorollback(String value) {
        this.norollback = value;
    }

    /**
     * Gets the value of the msisdnAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnAttr() {
        return msisdnAttr;
    }

    /**
     * Sets the value of the msisdnAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnAttr(String value) {
        this.msisdnAttr = value;
    }

    /**
     * Gets the value of the imsiAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsiAttr() {
        return imsiAttr;
    }

    /**
     * Sets the value of the imsiAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsiAttr(String value) {
        this.imsiAttr = value;
    }

}
