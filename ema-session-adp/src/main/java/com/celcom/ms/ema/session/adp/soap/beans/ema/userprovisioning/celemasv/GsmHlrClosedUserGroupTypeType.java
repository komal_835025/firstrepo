
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrClosedUserGroupTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrClosedUserGroupTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cugIndex">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="interlockCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="6"/>
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="restriction" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrCugRestrictionTypeType" minOccurs="0"/>
 *         &lt;element name="ts10" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrProvisionTypeType" minOccurs="0"/>
 *         &lt;element name="ts60" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrProvisionTypeType" minOccurs="0"/>
 *         &lt;element name="tsd0" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrProvisionTypeType" minOccurs="0"/>
 *         &lt;element name="bs20" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrProvisionTypeType" minOccurs="0"/>
 *         &lt;element name="bs30" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrProvisionTypeType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="cugIndex" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrClosedUserGroupTypeType", propOrder = {
    "cugIndex",
    "interlockCode",
    "restriction",
    "ts10",
    "ts60",
    "tsd0",
    "bs20",
    "bs30"
})
public class GsmHlrClosedUserGroupTypeType {

    @XmlElement(required = true)
    protected String cugIndex;
    @XmlElement(required = true)
    protected String interlockCode;
    protected String restriction;
    protected String ts10;
    protected String ts60;
    protected String tsd0;
    protected String bs20;
    protected String bs30;
    @XmlAttribute(name = "cugIndex", required = true)
    protected String cugIndexAttr;

    /**
     * Gets the value of the cugIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCugIndex() {
        return cugIndex;
    }

    /**
     * Sets the value of the cugIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCugIndex(String value) {
        this.cugIndex = value;
    }

    /**
     * Gets the value of the interlockCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterlockCode() {
        return interlockCode;
    }

    /**
     * Sets the value of the interlockCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterlockCode(String value) {
        this.interlockCode = value;
    }

    /**
     * Gets the value of the restriction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRestriction() {
        return restriction;
    }

    /**
     * Sets the value of the restriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRestriction(String value) {
        this.restriction = value;
    }

    /**
     * Gets the value of the ts10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTs10() {
        return ts10;
    }

    /**
     * Sets the value of the ts10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTs10(String value) {
        this.ts10 = value;
    }

    /**
     * Gets the value of the ts60 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTs60() {
        return ts60;
    }

    /**
     * Sets the value of the ts60 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTs60(String value) {
        this.ts60 = value;
    }

    /**
     * Gets the value of the tsd0 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTsd0() {
        return tsd0;
    }

    /**
     * Sets the value of the tsd0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTsd0(String value) {
        this.tsd0 = value;
    }

    /**
     * Gets the value of the bs20 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBs20() {
        return bs20;
    }

    /**
     * Sets the value of the bs20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBs20(String value) {
        this.bs20 = value;
    }

    /**
     * Gets the value of the bs30 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBs30() {
        return bs30;
    }

    /**
     * Sets the value of the bs30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBs30(String value) {
        this.bs30 = value;
    }

    /**
     * Gets the value of the cugIndexAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCugIndexAttr() {
        return cugIndexAttr;
    }

    /**
     * Sets the value of the cugIndexAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCugIndexAttr(String value) {
        this.cugIndexAttr = value;
    }

}
