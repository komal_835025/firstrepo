
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrMoclTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrMoclTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="asl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="bsl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ttp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="endAll" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrActivationTypeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrMoclTypeType", propOrder = {
    "asl",
    "bsl",
    "ttp",
    "endAll"
})
public class GsmHlrMoclTypeType {

    protected Boolean asl;
    protected Boolean bsl;
    protected Boolean ttp;
    protected String endAll;

    /**
     * Gets the value of the asl property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAsl() {
        return asl;
    }

    /**
     * Sets the value of the asl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAsl(Boolean value) {
        this.asl = value;
    }

    /**
     * Gets the value of the bsl property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBsl() {
        return bsl;
    }

    /**
     * Sets the value of the bsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBsl(Boolean value) {
        this.bsl = value;
    }

    /**
     * Gets the value of the ttp property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTtp() {
        return ttp;
    }

    /**
     * Sets the value of the ttp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTtp(Boolean value) {
        this.ttp = value;
    }

    /**
     * Gets the value of the endAll property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndAll() {
        return endAll;
    }

    /**
     * Sets the value of the endAll property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndAll(String value) {
        this.endAll = value;
    }

}
