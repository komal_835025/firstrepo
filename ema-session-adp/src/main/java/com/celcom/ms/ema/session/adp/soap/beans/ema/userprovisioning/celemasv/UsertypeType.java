
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for usertypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="usertypeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PREPAID"/>
 *     &lt;enumeration value="POSTPAID"/>
 *     &lt;enumeration value="RTP"/>
 *     &lt;enumeration value="MPGS"/>
 *     &lt;enumeration value="BB"/>
 *     &lt;enumeration value="MCP"/>
 *     &lt;enumeration value="MSP"/>
 *     &lt;enumeration value="BSSMNP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "usertypeType")
@XmlEnum
public enum UsertypeType {

    PREPAID,
    POSTPAID,
    RTP,
    MPGS,
    BB,
    MCP,
    MSP,
    BSSMNP;

    public String value() {
        return name();
    }

    public static UsertypeType fromValue(String v) {
        return valueOf(v);
    }

}
