
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCawSupplementaryServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCawSupplementaryServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="provisionState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrProvisionTypeType"/>
 *         &lt;element name="activationState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrActivationTypeType" minOccurs="0"/>
 *         &lt;element name="ts10" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrSimpleActivationStateType" minOccurs="0"/>
 *         &lt;element name="ts60" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrSimpleActivationStateType" minOccurs="0"/>
 *         &lt;element name="tsd0" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrSimpleActivationStateType" minOccurs="0"/>
 *         &lt;element name="bs20" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrSimpleActivationStateType" minOccurs="0"/>
 *         &lt;element name="bs30" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrSimpleActivationStateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCawSupplementaryServiceType", propOrder = {
    "provisionState",
    "activationState",
    "ts10",
    "ts60",
    "tsd0",
    "bs20",
    "bs30"
})
public class GsmHlrCawSupplementaryServiceType {

    @XmlElement(required = true)
    protected String provisionState;
    protected String activationState;
    protected GsmHlrSimpleActivationStateType ts10;
    protected GsmHlrSimpleActivationStateType ts60;
    protected GsmHlrSimpleActivationStateType tsd0;
    protected GsmHlrSimpleActivationStateType bs20;
    protected GsmHlrSimpleActivationStateType bs30;

    /**
     * Gets the value of the provisionState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisionState() {
        return provisionState;
    }

    /**
     * Sets the value of the provisionState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisionState(String value) {
        this.provisionState = value;
    }

    /**
     * Gets the value of the activationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationState() {
        return activationState;
    }

    /**
     * Sets the value of the activationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationState(String value) {
        this.activationState = value;
    }

    /**
     * Gets the value of the ts10 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public GsmHlrSimpleActivationStateType getTs10() {
        return ts10;
    }

    /**
     * Sets the value of the ts10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public void setTs10(GsmHlrSimpleActivationStateType value) {
        this.ts10 = value;
    }

    /**
     * Gets the value of the ts60 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public GsmHlrSimpleActivationStateType getTs60() {
        return ts60;
    }

    /**
     * Sets the value of the ts60 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public void setTs60(GsmHlrSimpleActivationStateType value) {
        this.ts60 = value;
    }

    /**
     * Gets the value of the tsd0 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public GsmHlrSimpleActivationStateType getTsd0() {
        return tsd0;
    }

    /**
     * Sets the value of the tsd0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public void setTsd0(GsmHlrSimpleActivationStateType value) {
        this.tsd0 = value;
    }

    /**
     * Gets the value of the bs20 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public GsmHlrSimpleActivationStateType getBs20() {
        return bs20;
    }

    /**
     * Sets the value of the bs20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public void setBs20(GsmHlrSimpleActivationStateType value) {
        this.bs20 = value;
    }

    /**
     * Gets the value of the bs30 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public GsmHlrSimpleActivationStateType getBs30() {
        return bs30;
    }

    /**
     * Sets the value of the bs30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrSimpleActivationStateType }
     *     
     */
    public void setBs30(GsmHlrSimpleActivationStateType value) {
        this.bs30 = value;
    }

}
