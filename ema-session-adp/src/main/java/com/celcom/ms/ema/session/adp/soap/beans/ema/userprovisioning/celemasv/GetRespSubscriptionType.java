
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetRespSubscriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetRespSubscriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType"/>
 *         &lt;element name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" minOccurs="0"/>
 *         &lt;element name="iccid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hlrSub" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}HlrResponseSubscriptionType" minOccurs="0"/>
 *         &lt;element name="fnxSub" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}FnxSubscriptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="msisdn" use="required" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}MSISDNType" />
 *       &lt;attribute name="imsi" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}IMSIType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetRespSubscriptionType", propOrder = {
    "msisdn",
    "imsi",
    "iccid",
    "hlrSub",
    "fnxSub"
})
public class GetRespSubscriptionType {

    @XmlElement(required = true)
    protected String msisdn;
    protected String imsi;
    protected String iccid;
    protected HlrResponseSubscriptionType hlrSub;
    protected FnxSubscriptionType fnxSub;
    @XmlAttribute(name = "msisdn", required = true)
    protected String msisdnAttr;
    @XmlAttribute(name = "imsi")
    protected String imsiAttr;

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the imsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * Sets the value of the imsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsi(String value) {
        this.imsi = value;
    }

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIccid() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIccid(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the hlrSub property.
     * 
     * @return
     *     possible object is
     *     {@link HlrResponseSubscriptionType }
     *     
     */
    public HlrResponseSubscriptionType getHlrSub() {
        return hlrSub;
    }

    /**
     * Sets the value of the hlrSub property.
     * 
     * @param value
     *     allowed object is
     *     {@link HlrResponseSubscriptionType }
     *     
     */
    public void setHlrSub(HlrResponseSubscriptionType value) {
        this.hlrSub = value;
    }

    /**
     * Gets the value of the fnxSub property.
     * 
     * @return
     *     possible object is
     *     {@link FnxSubscriptionType }
     *     
     */
    public FnxSubscriptionType getFnxSub() {
        return fnxSub;
    }

    /**
     * Sets the value of the fnxSub property.
     * 
     * @param value
     *     allowed object is
     *     {@link FnxSubscriptionType }
     *     
     */
    public void setFnxSub(FnxSubscriptionType value) {
        this.fnxSub = value;
    }

    /**
     * Gets the value of the msisdnAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnAttr() {
        return msisdnAttr;
    }

    /**
     * Sets the value of the msisdnAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnAttr(String value) {
        this.msisdnAttr = value;
    }

    /**
     * Gets the value of the imsiAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsiAttr() {
        return imsiAttr;
    }

    /**
     * Sets the value of the imsiAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsiAttr(String value) {
        this.imsiAttr = value;
    }

}
