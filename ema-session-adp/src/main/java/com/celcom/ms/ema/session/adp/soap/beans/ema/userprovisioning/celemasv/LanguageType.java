
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LanguageType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LanguageType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="English"/>
 *     &lt;enumeration value="Malay"/>
 *     &lt;enumeration value="Mandarin"/>
 *     &lt;enumeration value="Tamil"/>
 *     &lt;enumeration value="Bangladesh"/>
 *     &lt;enumeration value="Indonesian"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LanguageType")
@XmlEnum
public enum LanguageType {

    @XmlEnumValue("English")
    ENGLISH("English"),
    @XmlEnumValue("Malay")
    MALAY("Malay"),
    @XmlEnumValue("Mandarin")
    MANDARIN("Mandarin"),
    @XmlEnumValue("Tamil")
    TAMIL("Tamil"),
    @XmlEnumValue("Bangladesh")
    BANGLADESH("Bangladesh"),
    @XmlEnumValue("Indonesian")
    INDONESIAN("Indonesian");
    private final String value;

    LanguageType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LanguageType fromValue(String v) {
        for (LanguageType c: LanguageType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
