package com.celcom.ms.ema.session.adp.util;

import org.springframework.web.client.RestTemplate;


public class RefreshProperties {
	private RestTemplate restTemplate = new RestTemplate();
	//JSONObject jsonObject;
	public void refreshEndpoint() {
		System.out.println("*****************************Hitting rest endpoint*****************************");
		restTemplate.postForObject("http://localhost:9090/actuator/refresh",null,Object.class);
		System.out.println("*****************************After Hitting rest endpoint*****************************");
	}
}