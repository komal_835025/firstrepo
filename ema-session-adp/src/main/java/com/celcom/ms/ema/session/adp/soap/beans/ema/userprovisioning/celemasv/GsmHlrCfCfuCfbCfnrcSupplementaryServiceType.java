
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCfCfuCfbCfnrcSupplementaryServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCfCfuCfbCfnrcSupplementaryServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="provisionState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrProvisionTypeType"/>
 *         &lt;element name="activationState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrActivationTypeType" minOccurs="0"/>
 *         &lt;element name="fnum" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="subAddress" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="4"/>
 *               &lt;maxLength value="42"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ofa" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="511"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="keep" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="respSyntax" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ts10" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrFnumCfuCfbCfnrcActivationStateType" minOccurs="0"/>
 *         &lt;element name="ts60" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrFnumCfuCfbCfnrcActivationStateType" minOccurs="0"/>
 *         &lt;element name="tsd0" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrFnumCfuCfbCfnrcActivationStateType" minOccurs="0"/>
 *         &lt;element name="bs20" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrFnumCfuCfbCfnrcActivationStateType" minOccurs="0"/>
 *         &lt;element name="bs30" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrFnumCfuCfbCfnrcActivationStateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCfCfuCfbCfnrcSupplementaryServiceType", propOrder = {
    "provisionState",
    "activationState",
    "fnum",
    "subAddress",
    "ofa",
    "keep",
    "respSyntax",
    "ts10",
    "ts60",
    "tsd0",
    "bs20",
    "bs30"
})
public class GsmHlrCfCfuCfbCfnrcSupplementaryServiceType {

    @XmlElement(required = true)
    protected String provisionState;
    protected String activationState;
    protected String fnum;
    protected String subAddress;
    protected Integer ofa;
    protected Integer keep;
    protected Integer respSyntax;
    protected GsmHlrFnumCfuCfbCfnrcActivationStateType ts10;
    protected GsmHlrFnumCfuCfbCfnrcActivationStateType ts60;
    protected GsmHlrFnumCfuCfbCfnrcActivationStateType tsd0;
    protected GsmHlrFnumCfuCfbCfnrcActivationStateType bs20;
    protected GsmHlrFnumCfuCfbCfnrcActivationStateType bs30;

    /**
     * Gets the value of the provisionState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisionState() {
        return provisionState;
    }

    /**
     * Sets the value of the provisionState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisionState(String value) {
        this.provisionState = value;
    }

    /**
     * Gets the value of the activationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationState() {
        return activationState;
    }

    /**
     * Sets the value of the activationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationState(String value) {
        this.activationState = value;
    }

    /**
     * Gets the value of the fnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnum() {
        return fnum;
    }

    /**
     * Sets the value of the fnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnum(String value) {
        this.fnum = value;
    }

    /**
     * Gets the value of the subAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubAddress() {
        return subAddress;
    }

    /**
     * Sets the value of the subAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubAddress(String value) {
        this.subAddress = value;
    }

    /**
     * Gets the value of the ofa property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOfa() {
        return ofa;
    }

    /**
     * Sets the value of the ofa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOfa(Integer value) {
        this.ofa = value;
    }

    /**
     * Gets the value of the keep property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKeep() {
        return keep;
    }

    /**
     * Sets the value of the keep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKeep(Integer value) {
        this.keep = value;
    }

    /**
     * Gets the value of the respSyntax property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRespSyntax() {
        return respSyntax;
    }

    /**
     * Sets the value of the respSyntax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRespSyntax(Integer value) {
        this.respSyntax = value;
    }

    /**
     * Gets the value of the ts10 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public GsmHlrFnumCfuCfbCfnrcActivationStateType getTs10() {
        return ts10;
    }

    /**
     * Sets the value of the ts10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public void setTs10(GsmHlrFnumCfuCfbCfnrcActivationStateType value) {
        this.ts10 = value;
    }

    /**
     * Gets the value of the ts60 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public GsmHlrFnumCfuCfbCfnrcActivationStateType getTs60() {
        return ts60;
    }

    /**
     * Sets the value of the ts60 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public void setTs60(GsmHlrFnumCfuCfbCfnrcActivationStateType value) {
        this.ts60 = value;
    }

    /**
     * Gets the value of the tsd0 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public GsmHlrFnumCfuCfbCfnrcActivationStateType getTsd0() {
        return tsd0;
    }

    /**
     * Sets the value of the tsd0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public void setTsd0(GsmHlrFnumCfuCfbCfnrcActivationStateType value) {
        this.tsd0 = value;
    }

    /**
     * Gets the value of the bs20 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public GsmHlrFnumCfuCfbCfnrcActivationStateType getBs20() {
        return bs20;
    }

    /**
     * Sets the value of the bs20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public void setBs20(GsmHlrFnumCfuCfbCfnrcActivationStateType value) {
        this.bs20 = value;
    }

    /**
     * Gets the value of the bs30 property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public GsmHlrFnumCfuCfbCfnrcActivationStateType getBs30() {
        return bs30;
    }

    /**
     * Sets the value of the bs30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrFnumCfuCfbCfnrcActivationStateType }
     *     
     */
    public void setBs30(GsmHlrFnumCfuCfbCfnrcActivationStateType value) {
        this.bs30 = value;
    }

}
