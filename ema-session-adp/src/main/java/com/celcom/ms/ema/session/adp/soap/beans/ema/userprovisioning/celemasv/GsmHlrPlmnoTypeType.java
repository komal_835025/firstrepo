
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrPlmnoTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrPlmnoTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="activationState" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrActivationTypeType" minOccurs="0"/>
 *         &lt;element name="intId" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrIntIdTypeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrPlmnoTypeType", propOrder = {
    "activationState",
    "intId"
})
public class GsmHlrPlmnoTypeType {

    protected String activationState;
    protected GsmHlrIntIdTypeType intId;

    /**
     * Gets the value of the activationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationState() {
        return activationState;
    }

    /**
     * Sets the value of the activationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationState(String value) {
        this.activationState = value;
    }

    /**
     * Gets the value of the intId property.
     * 
     * @return
     *     possible object is
     *     {@link GsmHlrIntIdTypeType }
     *     
     */
    public GsmHlrIntIdTypeType getIntId() {
        return intId;
    }

    /**
     * Sets the value of the intId property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsmHlrIntIdTypeType }
     *     
     */
    public void setIntId(GsmHlrIntIdTypeType value) {
        this.intId = value;
    }

}
