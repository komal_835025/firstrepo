/**
 * 
 */
package com.celcom.ms.ema.session.adp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.celcom.ms.ema.session.adp.model.EMASessionControlResDTO;
import com.celcom.ms.ema.session.adp.model.ResponseStatusDTO;
import com.celcom.ms.ema.session.adp.service.EMASessionControlAdpService;
import com.celcom.ms.ema.session.adp.util.GetProperties;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/emaSessionControl")
public class EMASessionControlAdpController {

	private static final Logger logger = LoggerFactory.getLogger(EMASessionControlAdpController.class);

	@Autowired
	EMASessionControlAdpService emaSessionControlAdpService;
	
	@Autowired
	GetProperties properties;
	
	private static final String RESPONSE_STATUS = "Ok";
	private static final String RESPONSE_STATUS_DESC = "Success";
	private EMASessionControlResDTO response = null;

	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK - Request successfully executed"),
			@ApiResponse(code = 401, message = "You are not authorized to view this resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 503, message = "Service Unavailable") })
	@GetMapping("/v1/sessionId")
	@ResponseBody
	public EMASessionControlResDTO getSessionId() {
		logger.info(" EMASessionControlAdpController : executing getSessionId() ");
		
		System.out.println("Username:------------"+properties.getUsername());
		
		response = emaSessionControlAdpService.getSessionId();
		
		if(response != null && response.getSessionId() != null) {
			ResponseStatusDTO responseStatusDTO = new ResponseStatusDTO();
			responseStatusDTO.setStatus(RESPONSE_STATUS);
			responseStatusDTO.setStatusDesc(RESPONSE_STATUS_DESC);
			response.setResponseStatus(responseStatusDTO);
		}
		
		return response;
	}
	
	

}
