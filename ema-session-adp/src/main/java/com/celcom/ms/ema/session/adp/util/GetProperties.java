package com.celcom.ms.ema.session.adp.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
@RequestMapping("/properties")
public class GetProperties {
	@Value("${ema.session.control.login.username}")
	private String username;

	@Value("${ema.session.control.login.password}")
	private String password;
	
	@Value("${logging.level.root}")
	private String loggingLevel;
	
	@GetMapping("/getUsername")
	public String getUsername() {
		System.out.println("Username = "+username);
		return username;
	}

	@GetMapping("/getPassword")
	public String getPassword() {
		System.out.println("Password = "+password);
		return password;
	}

	@GetMapping("/getLoggingLevel")
	public String getLoggingLevel() {
		System.out.println("logging level = "+loggingLevel);
		return loggingLevel;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setLoggingLevel(String loggingLevel) {
		this.loggingLevel = loggingLevel;
	}
	
	
		
}