
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCCamelOctdp2TypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCCamelOctdp2TypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cch" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="2"/>
 *               &lt;maxInclusive value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mty" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrMtyTypeType" minOccurs="0"/>
 *         &lt;element name="dnum" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="17"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dlgh" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bs" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bsg" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ftc" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrFtcTypeType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCCamelOctdp2TypeType", propOrder = {
    "id",
    "cch",
    "mty",
    "dnum",
    "dlgh",
    "bs",
    "bsg",
    "ftc"
})
public class GsmHlrCCamelOctdp2TypeType {

    protected Integer id;
    protected Integer cch;
    protected String mty;
    protected String dnum;
    protected String dlgh;
    protected String bs;
    protected String bsg;
    protected String ftc;
    @XmlAttribute(name = "id", required = true)
    protected BigInteger idAttr;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the cch property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCch() {
        return cch;
    }

    /**
     * Sets the value of the cch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCch(Integer value) {
        this.cch = value;
    }

    /**
     * Gets the value of the mty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMty() {
        return mty;
    }

    /**
     * Sets the value of the mty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMty(String value) {
        this.mty = value;
    }

    /**
     * Gets the value of the dnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDnum() {
        return dnum;
    }

    /**
     * Sets the value of the dnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDnum(String value) {
        this.dnum = value;
    }

    /**
     * Gets the value of the dlgh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlgh() {
        return dlgh;
    }

    /**
     * Sets the value of the dlgh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlgh(String value) {
        this.dlgh = value;
    }

    /**
     * Gets the value of the bs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBs() {
        return bs;
    }

    /**
     * Sets the value of the bs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBs(String value) {
        this.bs = value;
    }

    /**
     * Gets the value of the bsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBsg() {
        return bsg;
    }

    /**
     * Sets the value of the bsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBsg(String value) {
        this.bsg = value;
    }

    /**
     * Gets the value of the ftc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFtc() {
        return ftc;
    }

    /**
     * Sets the value of the ftc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFtc(String value) {
        this.ftc = value;
    }

    /**
     * Gets the value of the idAttr property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIdAttr() {
        return idAttr;
    }

    /**
     * Sets the value of the idAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIdAttr(BigInteger value) {
        this.idAttr = value;
    }

}
