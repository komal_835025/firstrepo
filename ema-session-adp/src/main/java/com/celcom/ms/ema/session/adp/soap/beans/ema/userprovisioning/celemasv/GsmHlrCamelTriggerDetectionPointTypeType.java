
package com.celcom.ms.ema.session.adp.soap.beans.ema.userprovisioning.celemasv;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GsmHlrCamelTriggerDetectionPointTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GsmHlrCamelTriggerDetectionPointTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="triggeringPoint" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrTriggeringPointTypeType" minOccurs="0"/>
 *         &lt;element name="detectionPoint" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gsa" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="3"/>
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="serviceKey" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2147483647"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="defaultErrorHandling" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cch" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="i" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrITypeType" minOccurs="0"/>
 *         &lt;element name="dialnum" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="19"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="triggeringPoint" use="required" type="{http://schemas.ericsson.com/ema/UserProvisioning/CELEMASV/}GsmHlrTriggeringPointTypeType" />
 *       &lt;attribute name="detectionPoint" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="cch" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GsmHlrCamelTriggerDetectionPointTypeType", propOrder = {
    "triggeringPoint",
    "detectionPoint",
    "gsa",
    "serviceKey",
    "defaultErrorHandling",
    "cch",
    "i",
    "dialnum"
})
public class GsmHlrCamelTriggerDetectionPointTypeType {

    protected String triggeringPoint;
    protected String detectionPoint;
    protected String gsa;
    protected Integer serviceKey;
    protected Integer defaultErrorHandling;
    protected Integer cch;
    protected String i;
    protected String dialnum;
    @XmlAttribute(name = "triggeringPoint", required = true)
    protected String triggeringPointAttr;
    @XmlAttribute(name = "detectionPoint", required = true)
    protected String detectionPointAttr;
    @XmlAttribute(name = "cch", required = true)
    protected BigInteger cchAttr;

    /**
     * Gets the value of the triggeringPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTriggeringPoint() {
        return triggeringPoint;
    }

    /**
     * Sets the value of the triggeringPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTriggeringPoint(String value) {
        this.triggeringPoint = value;
    }

    /**
     * Gets the value of the detectionPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetectionPoint() {
        return detectionPoint;
    }

    /**
     * Sets the value of the detectionPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetectionPoint(String value) {
        this.detectionPoint = value;
    }

    /**
     * Gets the value of the gsa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGsa() {
        return gsa;
    }

    /**
     * Sets the value of the gsa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGsa(String value) {
        this.gsa = value;
    }

    /**
     * Gets the value of the serviceKey property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceKey() {
        return serviceKey;
    }

    /**
     * Sets the value of the serviceKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceKey(Integer value) {
        this.serviceKey = value;
    }

    /**
     * Gets the value of the defaultErrorHandling property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultErrorHandling() {
        return defaultErrorHandling;
    }

    /**
     * Sets the value of the defaultErrorHandling property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultErrorHandling(Integer value) {
        this.defaultErrorHandling = value;
    }

    /**
     * Gets the value of the cch property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCch() {
        return cch;
    }

    /**
     * Sets the value of the cch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCch(Integer value) {
        this.cch = value;
    }

    /**
     * Gets the value of the i property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getI() {
        return i;
    }

    /**
     * Sets the value of the i property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setI(String value) {
        this.i = value;
    }

    /**
     * Gets the value of the dialnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDialnum() {
        return dialnum;
    }

    /**
     * Sets the value of the dialnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDialnum(String value) {
        this.dialnum = value;
    }

    /**
     * Gets the value of the triggeringPointAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTriggeringPointAttr() {
        return triggeringPointAttr;
    }

    /**
     * Sets the value of the triggeringPointAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTriggeringPointAttr(String value) {
        this.triggeringPointAttr = value;
    }

    /**
     * Gets the value of the detectionPointAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetectionPointAttr() {
        return detectionPointAttr;
    }

    /**
     * Sets the value of the detectionPointAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetectionPointAttr(String value) {
        this.detectionPointAttr = value;
    }

    /**
     * Gets the value of the cchAttr property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCchAttr() {
        return cchAttr;
    }

    /**
     * Sets the value of the cchAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCchAttr(BigInteger value) {
        this.cchAttr = value;
    }

}
